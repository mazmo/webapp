import { NowRequest, NowResponse } from '@vercel/node';
// @ts-ignore
import extract from 'meta-extractor';

const LINK_TYPES = {
  LINK: 'LINK',
  IMAGE: 'IMAGE',
  GIF: 'GIF',
};

export default async function (req: NowRequest, res: NowResponse) {
  const url = req.query.url as string;

  // Check if the URL is a YouTube link
  if (url.match(/^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.be)/)) {
    const oembedUrl = `https://www.youtube.com/oembed?url=${encodeURIComponent(url)}&format=json`;
    const response = await fetch(oembedUrl);
    const oembedData = await response.json();

    return res.json({
      type: LINK_TYPES.LINK,
      url,
      title: oembedData.title,
      description: '',
      icon: oembedData.thumbnail_url,
      imageWidth: oembedData.thumbnail_width,
      imageHeight: oembedData.thumbnail_height,
      meta: {
        ...oembedData,
        imageWidth: oembedData.thumbnail_width,
        imageHeight: oembedData.thumbnail_height,
        ogVideoWidth: oembedData.width,
        ogVideoHeight: oembedData.height,
        ogVideoSecureUrl: (() => {
          const match = oembedData.html.match(/src="([^"]+)"/);
          return match ? match[1] : null;
        })(),
      },
    });
  }

  const data = await extract({ uri: url });

  if (data.file && data.file.mime.includes('image/')) {
    const type = data.file.mime === 'image/gif' ? LINK_TYPES.GIF : LINK_TYPES.IMAGE;
    return res.json({
      type,
      url: req.query.url,
      imageWidth: data.ogImageWidth,
      imageHeight: data.ogImageHeight,
      meta: data,
    });
  }

  return res.json({
    type: LINK_TYPES.LINK,
    url: data.ogUrl || req.query.url,
    title: data.ogTitle || data.title,
    description: data.ogDescription || data.description || data.twitterDescription || '',
    icon: data.ogImage || (data.images && data.images[0]) || null,
    imageWidth: data.ogImageWidth,
    imageHeight: data.ogImageHeight,
    meta: data,
  });
}
