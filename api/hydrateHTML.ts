import { DEFAULT_TITLE, DEFAULT_IMAGE, DEFAULT_DESCRIPTION } from './constants';

const hydrateHTML = (
  indexHTML: string,
  newTitle: string,
  newDescription?: string,
  newOgImage?: string,
  eventId?: number,
  canonicalUrl?: string,
) => {
  const escape = (string: string) => string.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
  const titleRegex = new RegExp(escape(DEFAULT_TITLE), 'g');
  const imageRegex = new RegExp(escape(DEFAULT_IMAGE), 'g');
  const descriptionRegex = new RegExp(escape(DEFAULT_DESCRIPTION), 'g');

  let hydratedHTML = indexHTML.replace(titleRegex, newTitle);

  if (newDescription) hydratedHTML = hydratedHTML.replace(descriptionRegex, newDescription);
  if (newOgImage) hydratedHTML = hydratedHTML.replace(imageRegex, newOgImage);

  const event = eventId ? `<meta name="mazmo:event" content="${eventId}" />` : '';
  hydratedHTML = hydratedHTML.replace('<meta name="mazmo:event" content="" />', event);

  if (canonicalUrl) {
    const canonical = `<link rel="canonical" href="${canonicalUrl}" />`;
    hydratedHTML = hydratedHTML.replace('<link rel="canonical" href="" />', canonical);
    hydratedHTML = hydratedHTML.replace('<link rel="canonical" href=""/>', canonical);
  } else {
    hydratedHTML = hydratedHTML.replace('<link rel="canonical" href="" />', '');
    hydratedHTML = hydratedHTML.replace('<link rel="canonical" href=""/>', '');
  }

  return hydratedHTML;
};

export default hydrateHTML;
