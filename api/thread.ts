import { NowRequest, NowResponse, NowRequestQuery } from '@vercel/node';
import axios from 'axios';
import fs from 'fs';

import { API_URI } from './constants';
import hydrateHTML from './hydrateHTML';

const getIndex = async (proto: string, host: string) => {
  if (process.env.NODE_ENV === 'development') {
    const data = fs.readFileSync(`${__dirname}/../public/index.html`, 'utf8');
    return data;
  }

  const { data } = await axios.get(`${proto}://${host}/index.html`);
  return data;
};

const getThread = async ({ communitySlug, threadSlug }: NowRequestQuery) => {
  const { data } = await axios.get(`${API_URI}/communities/${communitySlug}/threads/${threadSlug}`);
  return data;
};

const getCommunity = async ({ communitySlug }: NowRequestQuery) => {
  const { data } = await axios.get(`${API_URI}/communities/${communitySlug}`);
  return data;
};

export default async function (req: NowRequest, res: NowResponse) {
  try {
    const promises = [
      getIndex(String(req.headers['x-forwarded-proto'] || 'http'), String(req.headers.host)),
      getThread(req.query),
      getCommunity(req.query),
    ];

    const [indexHTML, thread, community] = await Promise.all(promises);

    const newTitle = `${thread.title} - ${community.name} - Mazmo`;
    const filenameEncoded = encodeURIComponent(`${community.name}<br />**${thread.title}**`);
    const newOgImage = thread.event ? thread.event.cover : `https://og-image.mazmo.net/${filenameEncoded}.jpeg?theme=dark&md=1&fontSize=75px&images=https%3A%2F%2Fmazmo.net%2Fimages%2Flogodark.png&heights=150`;
    const newDescription = thread.rawContent.replace(/"/g, '\\x22').replace(/(?:\r\n|\r|\n)/g, ' ').substring(0, 200).trim();

    const hydratedHTML = hydrateHTML(
      indexHTML,
      newTitle,
      newDescription,
      newOgImage,
      thread.eventId,
      `https://mazmo.net/+${community.slug}/${thread.slug}`,
    );

    res.send(hydratedHTML);
  } catch (error) {
    const indexHTML = await getIndex(String(req.headers['x-forwarded-proto'] || 'http'), String(req.headers.host));
    res.send(indexHTML);
  }
}
