import { NowRequest, NowResponse, NowRequestQuery } from '@vercel/node';
import axios from 'axios';
import fs from 'fs';

import hydrateHTML from './hydrateHTML';

const getIndex = async (proto: string, host: string) => {
  if (process.env.NODE_ENV === 'development') {
    const data = fs.readFileSync(`${__dirname}/../public/index.html`, 'utf8');
    return data;
  }

  const { data } = await axios.get(`${proto}://${host}/index.html`);
  return data;
};

const getProfile = async ({ username }: NowRequestQuery) => {
  const { data } = await axios.get(`https://prod.mazmoapi.net/users/${username}`);
  return data;
};

export default async function (req: NowRequest, res: NowResponse) {
  try {
    const promises = [
      getIndex(String(req.headers['x-forwarded-proto'] || 'http'), String(req.headers.host)),
      getProfile(req.query),
    ];

    const [indexHTML, user] = await Promise.all(promises);

    const newTitle = `${user.displayname} - Mazmo`;
    const filenameEncoded = encodeURI(`**${user.displayname}**`);
    const newOgImage = `https://og-image.mazmo.net/${filenameEncoded}.jpeg?theme=dark&md=1&fontSize=75px&images=https%3A%2F%2Fmazmo.net%2Fimages%2Flogodark.png&heights=150`;

    const hydratedHTML = hydrateHTML(indexHTML, newTitle, undefined, newOgImage);

    res.send(hydratedHTML);
  } catch (error) {
    const indexHTML = await getIndex(String(req.headers['x-forwarded-proto'] || 'http'), String(req.headers.host));
    res.send(indexHTML);
  }
}
