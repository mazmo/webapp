import { NowRequest, NowResponse } from '@vercel/node';
import axios from 'axios';

import { API_URI } from '../constants';

export default async function (req: NowRequest, res: NowResponse) {
  const url = req.url?.replace('/sitemaps/chat/', '') || '';
  const { data } = await axios.get(`${API_URI}/chat/sitemaps/${url}`);

  res.setHeader('content-type', 'text/xml');
  return res.send(data);
}
