/* eslint-disable */
const open = require('open');

function capitalize(text) {
  return text.charAt(0).toUpperCase() + text.slice(1);
}

function doOpen(file) {
  return new Promise(function(resolve, reject) {
    open(file)
      .then(function() { resolve('Opened ' + file); })
      .catch(function(error) { reject(error); });
  });
}

module.exports = function (plop) {
  plop.setHelper('capitalize', function (text) {
    return capitalize(text);
  });

  // Icon Component generator
  plop.setGenerator('icon', {
    description: 'Creates a new Icon Component',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'Icon name please',
    }],
    actions: [
      {
        type: 'add',
        path: 'src/components/Icons/{{capitalize name}}.js',
        templateFile: 'plops/icon.hbs',
      },
      {
        type: 'append',
        path: 'src/components/Icons/index.js',
        unique: true,
        templateFile: 'plops/index-named.hbs',
      },
      function openFile(answers) {
        return doOpen('src/components/Icons/' + capitalize(answers.name) + '.js');
      },
    ],
  });

  // Component generator
  plop.setGenerator('component', {
    description: 'Creates a new Component',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'Component name please',
    }],
    actions: [
      {
        type: 'add',
        path: 'src/components/{{capitalize name}}/{{capitalize name}}.js',
        templateFile: 'plops/component.hbs',
      },
      {
        type: 'add',
        path: 'src/components/{{capitalize name}}/index.js',
        templateFile: 'plops/index.hbs',
      },
      function openFile(answers) {
        return doOpen('src/components/' + capitalize(answers.name) + '/' + capitalize(answers.name) + '.js');
      },
    ],
  });
}
