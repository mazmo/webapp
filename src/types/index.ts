
export interface User {
  age: number;
  avatar: {
    default: string;
    '150w': {
      jpeg: string;
    };
    '300w': {
      jpeg: string;
    };
  };
  badges: [];
  banned: boolean;
  blocked: boolean;
  connections: UserConnections;
  country: Country;
  displayname: string;
  email: string;
  emailBounced: boolean;
  emailConfirmed: boolean;
  eventCount: number;
  followPublicationOnComment: boolean;
  followersCount: number;
  followingCount: number;
  gender: string;
  haveFire: boolean;
  id: number;
  isFollowingMe?: boolean;
  isOrganization: boolean;
  jwt: string;
  knowedCount: number;
  knowingCount: number;
  lastLogin: string;
  loading?: boolean;
  membership: string;
  pronoun: Pronoun;
  regdate: string;
  region: Region;
  relationships: [];
  sades: number;
  starMode: boolean;
  subscriptions: {
    channels: Channel[]
    communities: Community[]
  }
  superFiredMe: boolean;
  suspended: boolean;
  tags: string[];
  tagsDescription: {
    inUse: string[];
    purchased: string[];
    available: number;
  };
  username: string;
}

export interface Me extends User {
  birthdate: string;
  blocks: [];
  canReportPublications: boolean;
  checklist: {
    privacy: string;
    lists: [];
  };
  fires: {
    purchased: {
      total: number;
      used: number;
    };
  };
  flags: [];
  knowing: [];
  lists: [];
  onboarding: {
    presentation: boolean;
    chatRequest: boolean;
  };
  organizationAdmins: [];
  organizationTokens: [];
}

interface Country {
  id: string;
  name: string;
  isoCode: string;
}

interface Region {
  id: string;
  name: string;
}

interface UserConnections {
  lovense: [];
}

type Pronoun = 'MALE' | 'FEMALE' | 'NEUTRAL';


export interface Community {
  avatar: string;
  cover: string;
  createdAt: string;
  description: string;
  founder: User;
  hasEvents: boolean;
  id: string;
  lastUpdate: {
    at: string;
    by: number;
  };
  membershipCount: number;
  mods: CommunityMod[];
  name: string;
  online: [];
  privacy: string;
  replyCount: number;
  slug: string;
  threadCount: number;
  updatedAt: string;
}

export interface CommunityMod {
  approved: boolean;
  community: string;
  createdAt: string;
  deleted: boolean;
  id: string;
  role: 'OWNER' | 'MOD' | 'ADMIN';
  updatedAt: string;
  user: User;
}

export interface Channel {
  avatar: string;
  bans: string[];
  bots: Array<{
    id: string;
    bot: string;
  }>;
  createdAt: string;
  description: string;
  embed: {
    url: string;
    withToken: boolean;
  };
  id: string;
  lastMessage: string;
  name: string;
  participantCount: number;
  privacy: string;
  updatedAt: string;
}

export interface Event {
  address: {
    adminArea: string;
    country: string;
    _id: string;
  };
  assistanceCount: number;
  cover: string;
  createdAt: string;
  currency: string;
  date: string;
  deleted: boolean;
  description: string;
  id: string;
  instructions: string;
  isPublic: boolean;
  features: {
    assistance: boolean;
    transactional: boolean;
    earlyTickets: boolean;
  };
  kinks: string[];
  links: string[];
  name: string;
  organizers: Array<{
    userId: User['id'];
  }>;
  price: number;
  rsvpCount: number;
  showLocation: boolean;
  sponsors: [];
  updatedAt: string;
}

export interface Bot {
  avatar: string
  channelCount: number
  createdAt: string
  description?: string
  id: string
  loading?: boolean
  name: string
  ownerId: User['id']
  updatedAt: string
}

export interface Badge {
  value: number;
  fill: string;
  color: string;
}
