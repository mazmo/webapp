/* eslint-disable react-hooks/exhaustive-deps */

import { useState, useEffect } from 'react';

const useLocalStorage = (key: string) => {
  let localStorageItem;
  if (key) {
    localStorageItem = localStorage[key];
  }

  const [localState, updateLocalState] = useState(localStorageItem);
  function syncLocalStorage(event: StorageEvent) {
    if (event.key === key) {
      updateLocalState(event.newValue);
    }
  }
  useEffect(() => {
    window.addEventListener('storage', syncLocalStorage);
    return () => {
      window.removeEventListener('storage', syncLocalStorage);
    };
  }, []);
  return localState;
};

export default useLocalStorage;
