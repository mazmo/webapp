import { EffectCallback, useEffect } from 'react';

// eslint-disable-next-line
const useOnMount = (mount: EffectCallback) => useEffect(mount, []);

export default useOnMount;
