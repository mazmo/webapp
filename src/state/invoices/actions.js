import { normalize } from 'normalizr';

import Api from 'state/api';
import invoiceSchema from 'state/invoices/schema';

import {
  LOAD,
} from './constants';

export const load = (invoicelist = []) => (dispatch) => {
  const data = normalize(invoicelist, [invoiceSchema]);

  dispatch({
    type: LOAD,
    data: data.entities.invoices,
  });
};

export const getCoupon = code => async () => {
  const { data } = await Api.req.get(`/events/coupons/${code}`);
  return data;
};

export const redeemCoupon = (eventId, couponId) => async (dispatch) => {
  const { data: { invoice } } = await Api.req.put(`/events/${eventId}/coupon`, { couponId });
  const data = normalize(invoice, invoiceSchema);

  dispatch({
    type: LOAD,
    data: data.entities.invoices,
  });
};
