/* eslint-disable import/prefer-default-export */

export const suggestionsEqual = (listA, listB) => {
  if (listA.length !== listB.length) return false;
  return listA.every((sA, index) => sA.userId === listB[index].userId);
};
