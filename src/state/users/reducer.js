import produce from 'immer';

import {
  ADD,
  ADD_FAILED_USERNAME,
  ONLINE_LIST,
  ONLINE,
  OFFLINE,
  SUGGESTIONS_LOAD,
  SUGGESTIONS_CLEAR,
} from './constants';

export const initialState = {
  data: {},
  usernames: {},
  online: [],
  userFetchQueue: new Set(),
  userFetching: false,
  failedUsernames: [],
  suggestions: null,
};

const reducer = (state = initialState, action) => produce(state, (draft) => {
  switch (action.type) {
    case ADD:
      Object.values(action.data || {}).forEach((user) => {
        draft.usernames[user.username] = user.id;
        const stateOrgPoints = state.data[user.id]?.organizationPoints;
        const userOrgPoints = user.organizationPoints;
        let orgPoints;
        if (stateOrgPoints || userOrgPoints) {
          orgPoints = {
            ...stateOrgPoints,
            ...userOrgPoints,
          };
        }

        draft.data[user.id] = {
          ...state.data[user.id],
          ...user,
          organizationPoints: orgPoints,
        };
      });
      break;

    case ADD_FAILED_USERNAME:
      draft.failedUsernames.push(action.username);
      break;

    case ONLINE_LIST:
      draft.online = action.data;
      break;

    case ONLINE:
      draft.online.push(action.userId);
      break;

    case OFFLINE:
      draft.online.splice(draft.online.findIndex(id => id === action.userId), 1);
      break;

    case SUGGESTIONS_LOAD:
      draft.suggestions = action.data.list;
      break;

    case SUGGESTIONS_CLEAR:
      draft.suggestions = initialState.suggestions;
      break;

    default:
  }
});

export default reducer;
