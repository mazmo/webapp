import { useCallback, useState } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, useOpenClose } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import Modal from 'components/Modal';

import locales from '../i18n';

const Wrapper = styled.div`
  margin-top: 52px;
`;

const textTypes = {
  personal: {
    button: 'Turn into organization',
    confirm: 'Are you sure you want to turn your account into an organization?',
    toast: 'Your account is now an organization',
  },
  organization: {
    button: 'Turn into personal',
    confirm: 'Are you sure you want to turn your account into personal?',
    toast: 'Your account is now personal',
  },
};

const OrganizationSwitch = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const isOrganization = useSelector(authSelectors.isOrganization);
  const [isModalOpen, openModal, closeModal] = useOpenClose();
  const [saving, setSaving] = useState(false);

  const texts = isOrganization ? textTypes.organization : textTypes.personal;

  const confirm = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(authActions.updateOrganization(!isOrganization));
      dispatch(appActions.addToast(t(texts.toast)));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setSaving(false);
    closeModal();
  }, [dispatch, t, closeModal, isOrganization, texts]);

  return (
    <>
      <Wrapper>
        <Button
          color="white"
          fontColor="black"
          onClick={openModal}
        >
          {t(texts.button)}
        </Button>
      </Wrapper>

      {/* Modals */}
      {isModalOpen && (
        <Modal
          title={t(texts.button)}
          onCancel={closeModal}
          actions={[
            <Button key="profile-organization-confirm" onClick={confirm} loading={saving}>{t('global:Confirm')}</Button>,
          ]}
        >
          {t(texts.confirm)}
        </Modal>
      )}
    </>
  );
};

OrganizationSwitch.propTypes = {
};

OrganizationSwitch.defaultProps = {
};

export default OrganizationSwitch;
