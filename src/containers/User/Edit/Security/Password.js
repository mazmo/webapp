import { useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, useInputValue } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Input from 'components/Forms/Input';
import Button from 'components/Button';

import locales from '../i18n';

const Password = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const username = useSelector(authSelectors.selectUsername);

  const newPassword = useInputValue('');
  const newPasswordRepeat = useInputValue('');

  const [saving, setSaving] = useState(false);

  const save = useCallback(async () => {
    try {
      setSaving(true);

      if (newPassword.value !== newPasswordRepeat.value) {
        throw new Error(t('Passwords doesn\'t match'));
      }

      await dispatch(authActions.updatePassword(newPassword.value));
      history.replace(`/@${username}`);
      dispatch(appActions.addToast(t('Password updated')));
    } catch (error) {
      dispatch(appActions.addError(error));
      setSaving(false);
    }
  }, [dispatch, history, username, newPassword, newPasswordRepeat, t]);

  const buttonDisabled = !newPassword.value || !newPasswordRepeat.value;

  return (
    <div>
      <h2>{t('Change Password')}</h2>

      <Input placeholder={t('New Password')} {...newPassword} type="password" />
      <Input placeholder={t('Repeat New Password')} {...newPasswordRepeat} type="password" />

      <Button onClick={save} loading={saving} disabled={buttonDisabled}>{t('global:Save')}</Button>
    </div>
  );
};

Password.propTypes = {
};

Password.defaultProps = {
};

export default Password;
