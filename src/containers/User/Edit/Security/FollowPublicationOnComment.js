import { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Toggle from 'components/Toggle';

import locales from '../i18n';

const Tip = styled.div`
  font-size: 14px;
  color: #A8A8A8;
  margin-top: 8px;
`;

const H2 = styled.h2`
  margin-bottom: 8px;
`;

const FollowPublicationOnComment = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const active = useSelector(authSelectors.selectFollowPublicationOnComment);

  const [loading, setLoading] = useState(false);

  const onChange = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(authActions.updateFollowPublicationOnComment(!active));
      dispatch(appActions.addToast(t('Follow publication on comment updated')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(false);
  }, [dispatch, t, active]);

  return (
    <div>
      <H2>{t('Follow publication on comment')}</H2>

      <Toggle active={active} label={t('Active')} onChange={onChange} loading={loading} position="left" />
      <Tip>{t('When commenting a publication you will get notifications for posterior comments')}</Tip>
    </div>
  );
};

FollowPublicationOnComment.propTypes = {
};

FollowPublicationOnComment.defaultProps = {
};

export default FollowPublicationOnComment;
