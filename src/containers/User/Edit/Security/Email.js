import { useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, useInputValue } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Input from 'components/Forms/Input';
import Button from 'components/Button';

import locales from '../i18n';

const Email = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const username = useSelector(authSelectors.selectUsername);
  const currentEmail = useSelector(authSelectors.selectEmail);

  const email = useInputValue(currentEmail);

  const [saving, setSaving] = useState(false);

  const save = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(authActions.updateEmail(email.value.trim()));
      history.replace(`/@${username}`);
      dispatch(appActions.addToast(t('E-mail updated')));
    } catch (error) {
      dispatch(appActions.addError(error));
      setSaving(false);
    }
  }, [dispatch, history, username, email, t]);

  const buttonDisabled = email.value === currentEmail;

  return (
    <div>
      <h2>{t('E-mail')}</h2>

      <Input placeholder={t('E-mail')} {...email} />

      <Button onClick={save} loading={saving} disabled={buttonDisabled}>{t('global:Save')}</Button>
    </div>
  );
};

Email.propTypes = {
};

Email.defaultProps = {
};

export default Email;
