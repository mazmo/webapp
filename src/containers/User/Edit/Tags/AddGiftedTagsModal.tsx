import { useState } from 'react';
import {
  Button,
  ButtonGroup,
  Checkbox,
  HStack,
  Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay,
  Stack,
  Text,
} from '@chakra-ui/react';
import { useHistory } from 'react-router-dom';

import { useTranslation } from 'hooks';

import UserAvatar from 'components/UserAvatar';
import UserDisplayName from 'components/UserDisplayName';
import UserLink from 'components/UserLink';

import locales from '../i18n';

interface Props {
  tags: string[];
  fromUserId: number;
  add: (tag: string) => () => void;
}

const AddGiftedTagsModal: React.FC<Props> = ({ tags, fromUserId, add }) => {
  const history = useHistory();
  const { t } = useTranslation(locales);

  const [selected, setSelected] = useState(tags);
  const [isAdding, setIsAdding] = useState(false);

  const onClose = () => {
    history.replace('/user/edit/tags');
  };

  const onSubmit = async () => {
    setIsAdding(true);

    // eslint-disable-next-line no-restricted-syntax
    for (const tag of selected) {
      // eslint-disable-next-line no-await-in-loop
      await add(tag)();
    }

    setIsAdding(false);
    history.replace('/user/edit/tags');
  };

  return (
    <Modal isOpen onClose={onClose} size="2xl" isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalCloseButton />

        <ModalHeader>{t('Add gifted tags')}</ModalHeader>
        <ModalBody>
          <HStack>
            <UserLink userId={fromUserId}>
              <HStack alignItems="center" fontWeight="bold" textColor="brand.700" _hover={{ textDecoration: 'underline' }} textDecoration="underline">
                {/* @ts-ignore */}
                <UserAvatar userId={fromUserId} size="25px" />
                <UserDisplayName userId={fromUserId} />
              </HStack>
            </UserLink>
            {' '}
            <Text>{t('gifted you the following tags', { context: tags.length > 1 ? 'PLURAL' : 'SINGULAR' })}</Text>
          </HStack>

          <Stack mt={6} ml={4}>
            {tags.map((tag) => (
              <Checkbox
                key={tag}
                isChecked={selected.includes(tag)}
                onChange={(e) => setSelected((cv) => {
                  if (e.target.checked) {
                    return [...cv, tag];
                  }
                  return cv.filter((tt) => tt !== tag);
                })}
              >
                {t(`global:TAG.${tag}`)}
              </Checkbox>
            ))}
          </Stack>
        </ModalBody>

        <ModalFooter mt={4}>
          <ButtonGroup isDisabled={isAdding}>
            <Button variant="outline" onClick={onClose}>{t('global:Cancel')}</Button>
            <Button
              variant="primary"
              isDisabled={!selected.length}
              isLoading={isAdding}
              onClick={onSubmit}
            >
              {t('Add selected tags', { context: selected.length > 1 || !selected.length ? 'PLURAL' : 'SINGULAR' })}
            </Button>
          </ButtonGroup>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default AddGiftedTagsModal;
