import PurchaseModal from 'components/PurchaseButton/PurchaseModal';

interface Props {
  add: (t: string) => () => void;
  purchase: any;
  onClose: () => void;
}

const PurchaseTagModal: React.FC<Props> = ({ purchase, onClose, add }) => {
  const itemName = purchase.needsPurchase ? `USERTAG.${purchase.tag}` : 'TAG_SLOT';

  let extraSlots = 0;
  if (purchase.needsPurchase && purchase.needsSlot) extraSlots = purchase.needsSlot;
  if (!purchase.needsPurchase && purchase.needsSlot > 1) extraSlots = purchase.needsSlot - 1;

  const afterPurchase = (tag: string) => async () => {
    // Add the tag
    return add(tag)();
  };

  return (
    <PurchaseModal
      itemName={itemName}
      otherItems={(new Array(extraSlots)).fill('TAG_SLOT')}
      close={onClose}
      afterPurchase={afterPurchase(purchase.tag)}
      showSuccessToast={false}
    />
  );
};

export default PurchaseTagModal;
