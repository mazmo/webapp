import { useState } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { Trans } from 'react-i18next';
import { useLocation } from 'react-router-dom';
import qs from 'qs';
import {
  Box, Divider, HStack, Tag, TagCloseButton, TagLabel, Text, Wrap,
} from '@chakra-ui/react';

import { useTranslation } from 'hooks';
import useToast from 'hooks/useToast';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';

import H2 from 'ui/H2';
import DraggableArea from 'components/DraggableArea';
import TagsList from 'components/TagsList';
import ShareButton from 'components/ShareButton';

import PurchaseTagModal from './PurchaseTagModal';
import AddGiftedTagsModal from './AddGiftedTagsModal';
import RecentlyAddedTagsAlert from './RecentlyAddedTagsAlert';
import locales from '../i18n';

const Tags = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const toast = useToast();
  const location = useLocation();
  const queryparams = qs.parse(location.search, { ignoreQueryPrefix: true });

  const tags = useSelector(authSelectors.selectTags, shallowEqual);
  const [recentlyAddedTags, setRecentlyAddedTags] = useState([]);
  const purchased = useSelector(authSelectors.tagsPurchased);
  const pronoun = useSelector(authSelectors.selectPronoun);
  const username = useSelector(authSelectors.selectUsername);

  const [adding, setAdding] = useState(null);
  const [removing, setRemoving] = useState(null);
  const [purchase, setPurchase] = useState(null);

  const closeTagPurchase = () => setPurchase(null);

  const add = (tag) => async () => {
    try {
      setAdding(tag);

      await dispatch(authActions.addTag(tag));
      toast.success(t('Tag successfully added'));
      setRecentlyAddedTags((cv) => [...cv, tag]);
    } catch (error) {
      if (error.response?.status === 400 && error.response?.data?.message === 'NEEDS_PURCHASE') {
        setPurchase({ ...error.response.data.data, tag });
      } else {
        toast.error(error);
      }
    }

    setAdding(null);
  };

  const remove = (tag) => async () => {
    try {
      setRemoving(tag);

      await dispatch(authActions.removeTag(tag));
      toast.success(t('Tag removed'));
      setRecentlyAddedTags((cv) => cv.filter((tt) => tt !== tag));
    } catch (error) {
      toast.error(error);
    }

    setRemoving(null);
  };

  const sortTags = async (newTags) => {
    try {
      await dispatch(authActions.sortTags(newTags.map(tag => tag.content)));
    } catch (error) {
      // Do nothing. Fail quietly.
    }
  };

  const selectedTags = tags
    .filter((tag) => tag !== removing)
    .map(tag => ({ id: tag, content: tag }));

  const imageneratorParams = new URLSearchParams();
  imageneratorParams.append('username', username);
  imageneratorParams.append('width', '540');
  imageneratorParams.append('height', '960');
  const imgShareUrl = `https://imagenerator.mazmo.net/newtags?${imageneratorParams.toString()}`;

  return (
    <>
      <Box>
        <Trans t={t} i18nKey="tags.explanation" ns="userEdit">
          <Text mb={4}>
            Choose the tags that you feel define your sexuality.
            You can change them as many times as you want.
          </Text>
        </Trans>

        <Box>
          <HStack justifyContent="space-around" alignItems="center" mb={4}>
            <H2 mb={0}>{t('Select and sort your tags')}</H2>
            <ShareButton imgUrl={imgShareUrl} />
          </HStack>

          {(recentlyAddedTags.length > 0 || true) && (
            <RecentlyAddedTagsAlert tags={recentlyAddedTags} />
          )}

          {selectedTags.length > 0 && (
            <Wrap>
              <DraggableArea
                mb={2}
                tags={selectedTags}
                render={({ tag }) => (
                  <Tag key={`usertag-general-${tag.content}`} mr={4} size="lg" colorScheme="red">
                    <TagLabel>{t(`global:TAG.${tag.content}`)}</TagLabel>
                    <TagCloseButton onClick={remove(tag.content)} />
                  </Tag>
                )}
                onChange={sortTags}
              />
            </Wrap>
          )}

          <Divider my={4} />

          <Box mt={6}>
            <TagsList
              filter={(tag) => !tags.includes(tag.label)}
              withBorderColor={(tag) => tag.price > 0 && !purchased.includes(tag.label)}
              onClick={add}
              addingLabel={adding}
              pronoun={pronoun}
            />
          </Box>
        </Box>
      </Box>

      {/* Modals */}
      {!!purchase && (
        <PurchaseTagModal purchase={purchase} onClose={closeTagPurchase} add={add} />
      )}
      {queryparams.tags && queryparams.tags.length > 0 && (
        <AddGiftedTagsModal
          tags={Array.isArray(queryparams.tags) ? queryparams.tags : [queryparams.tags]}
          fromUserId={parseInt(queryparams.giftFromUserId, 10)}
          add={add}
        />
      )}
    </>
  );
};

Tags.propTypes = {
};

Tags.defaultProps = {
};

export default Tags;
