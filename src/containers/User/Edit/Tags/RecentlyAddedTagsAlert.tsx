import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Box, Button, ButtonGroup,
  Text,
} from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import ShareButton from 'components/ShareButton';

import locales from '../i18n';

interface Props {
  tags: string[];
}

const RecentlyAddedTagsAlert: React.FC<Props> = ({ tags }) => {
  const { t } = useTranslation(locales);

  const username = useSelector(authSelectors.selectUsername);

  const imageneratorParams = new URLSearchParams();
  imageneratorParams.append('username', username);
  imageneratorParams.append('width', '430');
  imageneratorParams.append('height', '600');
  imageneratorParams.append('newtags', tags.join(','));
  const url = `https://imagenerator.mazmo.net/newtags?${imageneratorParams.toString()}`;
  imageneratorParams.delete('width');
  imageneratorParams.delete('height');
  imageneratorParams.append('width', '540');
  imageneratorParams.append('height', '960');
  const imgShareUrl = `https://imagenerator.mazmo.net/newtags?${imageneratorParams.toString()}`;

  const pascalCase = (str: string) => str
    .toLowerCase()
    .split(/\s+/)
    .map(word => word.charAt(0).toUpperCase() + word.slice(1))
    .join('');

  const params = new URLSearchParams();
  params.append('url', url);
  params.append('content', `:label: #NuevasEtiquetas ${tags.map(tag => `#${pascalCase(t(`global:TAG.${tag}`))}`).join(' ')}`);

  return (
    <Alert status="success" mb={4}>
      <AlertIcon mr={4} />
      <Box w="full">
        <AlertTitle>{t('New Tags!')}</AlertTitle>
        <AlertDescription>
          <Text fontSize="sm">{t('You can post to your feed to let everyone know about your new tags.')}</Text>

          <ButtonGroup mt={2} size="sm">
            <Link to={`/new/url?${params.toString()}`}><Button variant="primary">{t('Create publication')}</Button></Link>
            <ShareButton imgUrl={imgShareUrl} />
          </ButtonGroup>
        </AlertDescription>
      </Box>
    </Alert>
  );
};

export default RecentlyAddedTagsAlert;
