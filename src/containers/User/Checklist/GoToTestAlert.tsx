import {
  useEffect, useLayoutEffect, useState,
} from 'react';
import {
  Alert, AlertDescription, AlertIcon, AlertTitle, Button,
} from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import { UseFormWatch } from 'react-hook-form';
import { z } from 'zod';

import { useTranslation } from 'hooks';

import locales from 'containers/Profiles/Checklist/i18n';

import { checklistSelectionsFormSchema } from './ChecklistForm';

interface Props {
  watch: UseFormWatch<z.infer<typeof checklistSelectionsFormSchema>>;
}

const SHOW_TRESHOLD = 25;

const GoToTest: React.FC<Props> = ({ watch }) => {
  const { t } = useTranslation(locales);
  const selections = watch('selections');

  const total = Object.keys(selections).length;

  const completed = Object.values(selections).filter((selection) => (
    selection.experience !== ''
    && selection.interest !== ''
    && typeof selection.interest !== 'undefined'
    && typeof selection.experience !== 'undefined'
  )).length;
  const progress = total > 0 ? (completed / total) * 100 : 0;

  const [readyToTake, setReadyToTake] = useState(progress >= SHOW_TRESHOLD);

  useEffect(() => {
    setReadyToTake(progress >= SHOW_TRESHOLD);
  }, [progress]);

  useLayoutEffect(() => {
    if (readyToTake && typeof window !== 'undefined') {
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
  }, [readyToTake]);

  if (!readyToTake) {
    return (
      <Alert variant="default" mb={4} flexDirection="column" alignItems="center" justifyContent="center" textAlign="center">
        <AlertIcon m={0} />
        <AlertTitle mx={0} mb={2}>{t('KinkyTest not available')}</AlertTitle>
        <AlertDescription m={0}>
          {t('Complete at least a 25% of your checklist to see your results')}
        </AlertDescription>
      </Alert>
    );
  }

  return (
    <Alert variant="default" mb={4} flexDirection="column" alignItems="center" justifyContent="center" textAlign="center">
      <AlertIcon m={0} />
      <AlertTitle mx={0} mb={2}>{t('KinkyTest available!')}</AlertTitle>
      <AlertDescription m={0}>
        <Button as={Link} to="/kinkytest" size="sm" ml={2}>{t('See your results')}</Button>
      </AlertDescription>
    </Alert>
  );
};

export default GoToTest;
