import { useState } from 'react';
import {
  Icon, IconButton, Menu, MenuButton, MenuItemOption, MenuList,
  MenuOptionGroup,
  useDisclosure,
} from '@chakra-ui/react';
import { GlobeLock } from 'lucide-react';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import useToast from 'hooks/useToast';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';

import locales from 'containers/Profiles/Checklist/i18n';
import PrivacyListModal from './PrivacyListModal';

const PrivacyMenu: React.FC = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const toast = useToast();

  const checklistPrivacy = useSelector(authSelectors.selectChecklistPrivacy);

  const [isSaving, setIsSaving] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onPrivacyChange = async (value: string | string[]) => {
    try {
      setIsSaving(true);

      if (typeof value === 'string') {
        if (value !== 'LISTS') {
          await dispatch(authActions.updateChecklistPrivacy(value));
          toast.success(t('Checklist privacy updated'));
        }
      }
    } catch (error) {
      toast.error(error);
    }

    setIsSaving(false);
  };

  return (
    <>
      <Menu>
        <MenuButton as={IconButton} icon={<Icon as={GlobeLock} />} isLoading={isSaving} />
        <MenuList>
          <MenuOptionGroup title={t('Privacy')} type="radio" value={checklistPrivacy} onChange={onPrivacyChange}>
            <MenuItemOption value="PUBLIC">{t('PRIVACY.PUBLIC')}</MenuItemOption>
            <MenuItemOption value="CONTACTS">{t('PRIVACY.CONTACTS')}</MenuItemOption>
            <MenuItemOption value="LISTS" onClick={onOpen}>{t('PRIVACY.LISTS')}</MenuItemOption>
          </MenuOptionGroup>
        </MenuList>
      </Menu>

      <PrivacyListModal isOpen={isOpen} onClose={onClose} setIsSaving={setIsSaving} />
    </>
  );
};

export default PrivacyMenu;
