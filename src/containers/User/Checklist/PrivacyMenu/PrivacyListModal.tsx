import { useEffect } from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  Stack,
  Checkbox,
  ButtonGroup,
} from '@chakra-ui/react';
import z from 'zod';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import useToast from 'hooks/useToast';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';

import locales from 'containers/Profiles/Checklist/i18n';

interface Props {
  isOpen: boolean;
  onClose: () => void;
  setIsSaving: (isSaving: boolean) => void;
}

const listsFormSchema = z.object({
  lists: z.array(z.string()),
});

const PrivacyListModal: React.FC<Props> = ({ isOpen, onClose, setIsSaving }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const toast = useToast();

  const checklistLists = useSelector(authSelectors.selectChecklistListIds, shallowEqual);
  const usersLists: any[] = useSelector(authSelectors.selectUserLists, shallowEqual);

  const { handleSubmit, register, reset } = useForm<z.infer<typeof listsFormSchema>>({
    resolver: zodResolver(listsFormSchema),
    defaultValues: { lists: checklistLists },
  });

  useEffect(() => {
    if (isOpen) {
      reset();
    }
  }, [isOpen, reset]);

  const onListsSubmit = async (data: z.infer<typeof listsFormSchema>) => {
    try {
      onClose();
      setIsSaving(true);

      await dispatch(authActions.updateChecklistPrivacy('LISTS', data.lists));
      toast.success(t('Checklist privacy updated'));
      reset({ lists: data.lists });
    } catch (error) {
      toast.error(error);
    }

    setIsSaving(false);
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose} size="sm">
      <ModalOverlay />

      <form onSubmit={handleSubmit(onListsSubmit)}>
        <ModalContent>
          <ModalHeader>{t('Your users lists')}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Stack>
              {usersLists.map((list) => (
                <Checkbox key={list.id} value={list.id} {...register('lists')}>
                  {list.name}
                </Checkbox>
              ))}
            </Stack>
          </ModalBody>

          <ModalFooter>
            <ButtonGroup>
              <Button onClick={onClose}>{t('global:Cancel')}</Button>
              <Button variant="primary" type="submit">{t('global:Save')}</Button>
            </ButtonGroup>
          </ModalFooter>
        </ModalContent>
      </form>
    </Modal>
  );
};

export default PrivacyListModal;
