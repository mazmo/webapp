import { Box, Button, HStack } from '@chakra-ui/react';
import { useSelector } from 'react-redux';

import api from 'state/api';
import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import locales from 'containers/Profiles/Checklist/i18n';

import LoadingChecklistContent from './LoadingChecklistContent';
import Progress from './Progress';
import GoToTestAlert from './GoToTestAlert';
import FilterPills from './FilterPills';
import ChecklistForm, { ChecklistItem, Experience, Interest } from './ChecklistForm';
import { useChecklistForm } from './useChecklistForm';

type ServerSelectionStructure = {
  category: string;
  selections: {
    experience: Experience;
    interest: Interest;
    item: {
      id: string;
      name: string;
      relatesTo: string;
    };
  }[];
}[];

const ChecklistContent: React.FC = () => {
  const { t } = useTranslation(locales);
  const username = useSelector(authSelectors.selectUsername);

  const defaultValues = async () => {
    const { data } = await api.req!.get<ServerSelectionStructure>(`/users/${username}/checklist`);
    const defaultSelections: Record<string, { interest: Interest, experience: Experience }> = {};
    data.forEach(({ selections }) => {
      selections.forEach(({ item, experience, interest }) => {
        defaultSelections[item.id] = { experience, interest };
      });
    });

    return { selections: defaultSelections };
  };
  const {
    form: {
      handleSubmit, register, watch, formState: {
        isDirty, isLoading, isSubmitting,
      },
    },
    checklistStructure,
    typeFilters,
    setTypeFilters,
    isLoadingStructure,
    onSubmit,
  } = useChecklistForm({ defaultValues });

  if (isLoadingStructure || !checklistStructure || isLoading) return <LoadingChecklistContent />;

  return (
    <Box>
      <GoToTestAlert watch={watch} />

      <FilterPills filters={typeFilters} setFilters={setTypeFilters} />

      <form onSubmit={handleSubmit(onSubmit)}>
        <ChecklistForm
          checklistStructure={checklistStructure}
          itemFilter={(item: ChecklistItem) => typeFilters.includes(item.type)}
          register={register}
        />

        <HStack justifyContent="space-between" position="sticky" bottom={0} bgColor="white" py={4}>
          <Progress watch={watch} />
          <Button variant="primary" type="submit" isDisabled={!isDirty} isLoading={isSubmitting}>
            {t('global:Save')}
          </Button>
        </HStack>
      </form>
    </Box>
  );
};

export default ChecklistContent;
