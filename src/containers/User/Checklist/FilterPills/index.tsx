import { Dispatch, SetStateAction } from 'react';
import { HStack } from '@chakra-ui/react';

import FilterPill from './FilterPill';

interface Props {
  filters: ('TOP' | 'BOTTOM' | null)[];
  setFilters: Dispatch<SetStateAction<('TOP' | 'BOTTOM' | null)[]>>;
}

const FilterPills: React.FC<Props> = ({ filters, setFilters }) => {
  return (
    <HStack spacing={4} mb={4}>
      <FilterPill checked={filters} setChecked={setFilters} pillKey="TOP" />
      <FilterPill checked={filters} setChecked={setFilters} pillKey="BOTTOM" />
      <FilterPill checked={filters} setChecked={setFilters} pillKey={null} />
    </HStack>
  );
};

export default FilterPills;
