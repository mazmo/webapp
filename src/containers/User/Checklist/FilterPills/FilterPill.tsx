import { Dispatch, SetStateAction } from 'react';
import { Checkbox, Tag, TagLabel } from '@chakra-ui/react';

interface Props {
  checked: ('TOP' | 'BOTTOM' | null)[];
  setChecked: Dispatch<SetStateAction<('TOP' | 'BOTTOM' | null)[]>>;
  pillKey: 'TOP' | 'BOTTOM' | null;
}

const FilterPill: React.FC<Props> = ({ checked, setChecked, pillKey }) => {
  const isChecked = checked.includes(pillKey);
  let label = 'Top';
  if (pillKey === 'BOTTOM') label = 'Bottom';
  if (pillKey === null) label = 'Neutral';

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const nowChecked = event.target.checked;
    setChecked((cv) => {
      if (nowChecked) return [...cv, pillKey];
      return cv.filter((key) => key !== pillKey);
    });
  };

  return (
    <Tag size="lg" variant={isChecked ? 'solid' : 'outline'}>
      <TagLabel><Checkbox isChecked={isChecked} onChange={onChange}>{label}</Checkbox></TagLabel>
    </Tag>
  );
};

export default FilterPill;
