import { useEffect, useState, useCallback } from 'react';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import {
  Skeleton,
  Stack,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Box,
  Text,
  HStack,
} from '@chakra-ui/react';

import Api from 'state/api';
import { useTranslation, useTitle } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';

import Sidebar from 'containers/Sidebar';
import Layout from 'components/Layout';
import { FlexWrapper, FlexInnerWrapper } from 'components/FlexWrapper';
import PageTitle from 'components/PageTitle';
import EmptyState from 'components/EmptyState';
import Pagination from 'components/Pagination';
import PurchaseSadesModal from 'components/PurchaseSadesModal';

import NewTransfer from './NewTransfer';
import FromTo from './FromTo';
import Concept from './Concept';
import Coupons from './Coupons';
import Balance from './Balance';
import locales from './i18n';

const UserSades = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  useTitle(t('global:Sades'));
  const history = useHistory();

  const isPurchaseModalOpen = useRouteMatch('/user/sades/buy');

  const closePurchaseModal = useCallback(() => {
    history.push('/user/sades');
  }, [history]);

  const myId = useSelector(authSelectors.selectId);

  const [data, setData] = useState({});

  const loadPage = useCallback(async (page) => {
    setData(({ documents, ...p }) => p);
    const { data: result } = await Api.req.get('/bank/transactions', { params: { page, limit: 15 } });
    setData(result);
  }, []);

  useEffect(() => {
    loadPage(1);
    dispatch(appActions.uiLeftColumn(true));
  }, [loadPage, dispatch]);

  const { documents, ...pagination } = data;

  return (
    <>
      <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
        <FlexWrapper canOverflow>
          <FlexInnerWrapper>
            <PageTitle>{t('global:Sades')}</PageTitle>

            <Balance />

            <Coupons />

            <Box mt={5}>
              {documents ? (
                <>
                  {documents.length > 0 ? (
                    <Table>
                      <Thead>
                        <Tr>
                          <Th pl={0}>De/A</Th>
                          <Th>Concepto</Th>
                          <Th isNumeric>Cantidad</Th>
                          <Th pr={0}>Fecha</Th>
                        </Tr>
                      </Thead>
                      <Tbody>
                        {documents.map((transaction) => {
                          const outgoing = transaction.from.owner.type === 'USER' && transaction.from.owner.id === myId;

                          return (
                            <Tr key={`transaction-${transaction.id}`}>
                              <Td pl={0}><FromTo transaction={transaction} /></Td>
                              <Td><Concept transaction={transaction} /></Td>
                              <Td isNumeric>
                                <strong>{`§${transaction.amount.toFixed(2)}`}</strong>
                                {outgoing && transaction.tax > 0 && <span>{` (+§${transaction.tax})`}</span>}
                              </Td>
                              <Td pr={0}><Text fontSize="sm" color="gray.500">{moment(transaction.createdAt).format('D/M/YY HH:mm')}</Text></Td>
                            </Tr>
                          );
                        })}
                      </Tbody>
                    </Table>
                  ) : (
                    <EmptyState title={t('No transactions yet')} />
                  )}
                </>
              ) : (
                <Stack w="full" spacing={4}>
                  {Array.from({ length: 12 }).map(() => (
                    <Skeleton key={`skeleton-${Math.random().toString(36).substring(7)}`} height={12} />
                  ))}
                </Stack>
              )}
            </Box>

            {typeof pagination.current !== 'undefined' && pagination.pages > 0 && (
              <HStack mt={4} justifyContent="space-between">
                <NewTransfer />
                <Pagination {...pagination} onChange={loadPage} disabled={!!documents} />
              </HStack>
            )}

          </FlexInnerWrapper>
        </FlexWrapper>

        <Sidebar />
      </Layout>

      {isPurchaseModalOpen && (
        <PurchaseSadesModal onCancel={closePurchaseModal} onClose={closePurchaseModal} />
      )}
    </>
  );
};

UserSades.propTypes = {
};

UserSades.defaultProps = {
};

export default UserSades;
