import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Trans } from 'react-i18next';

import { useTranslation, useOpenClose } from 'hooks';

import Modal from 'components/Modal';
import Button from 'components/Button';

import locales from './i18n';

const Detail = styled.span`
  button {
    border: 0;
    background: transparent;
    color: ${props => props.theme.colors.main};
    cursor: pointer;
  }
`;
Detail.displayName = 'Detail';

const Concept = ({ transaction }) => {
  const { t } = useTranslation(locales);

  const [isDetailOpen, openDetail, closeDetail] = useOpenClose();

  if (transaction.concept === 'POOR_LOGIN' && transaction.to.owner.type === 'CENTRAL_BANK') {
    return <span>{t('POOR_LOGIN')}</span>;
  }

  if (transaction.concept === 'BOUGHT_CREDIT' && transaction.from.owner.type === 'CENTRAL_BANK') {
    return <span>{t('BOUGHT_CREDIT')}</span>;
  }

  if (transaction.concept === 'THREAD_AWARDED' && transaction.from.owner.type === 'CENTRAL_BANK') {
    return (
      <Trans t={t} ns="UserSades" i18nKey="thread.awarded">
        One of your
        {' '}
        <Link to={transaction.data.threadUrl}>Threads</Link>
        {' '}
        was awarded
      </Trans>
    );
  }

  if (transaction.concept === 'REPLY_AWARDED' && transaction.from.owner.type === 'CENTRAL_BANK') {
    return (
      <Trans t={t} ns="UserSades" i18nKey="reply.awarded">
        One of your
        {' '}
        <Link to={transaction.data.threadUrl}>Replies</Link>
        {' '}
        was awarded
      </Trans>
    );
  }

  if (transaction.type === 'WEEKLY' && transaction.from.owner.type === 'CENTRAL_BANK') {
    return (
      <>
        <Detail>
          {t('Weekly assignation')}
          <br />
          <button type="button" className="empty" onClick={openDetail}>{t('detail')}</button>
        </Detail>

        {isDetailOpen && (
          <Modal
            title={t('Weekly assignation')}
            actions={[
              <Button className="empty" key="weekly-detail" to="https://mazmo.net/+ayuda-y-sugerencias/rsnvscjwc">{t('Know more')}</Button>,
              <Button key="weekly-close" onClick={closeDetail}>{t('global:Close')}</Button>,
            ]}
          >
            <ul>
              <li>{t('Units by Spanks {{spanks}}', { spanks: transaction.data.spanksUnits.toFixed(2) })}</li>
              <li>{t('Units by Comments {{comments}}', { comments: transaction.data.commentsUnits.toFixed(2) })}</li>
              <li>{t('Units by Replies {{replies}}', { replies: transaction.data.repliesUnits.toFixed(2) })}</li>
            </ul>

            <br />

            <div><strong>{t('Total units {{units}}', { units: transaction.data.units.toFixed(2) })}</strong></div>
            <div>{t('AUV {{auv}}', { auv: transaction.data.auv })}</div>
          </Modal>
        )}
      </>
    );
  }
  return <span>{transaction.concept}</span>;
};

Concept.propTypes = {
  transaction: PropTypes.shape({
    concept: PropTypes.string,
    type: PropTypes.string.isRequired,
    from: PropTypes.shape({
      owner: PropTypes.shape({
        type: PropTypes.string,
      }),
    }),
    to: PropTypes.shape({
      owner: PropTypes.shape({
        type: PropTypes.string,
      }),
    }),
    data: PropTypes.shape({
      spanksUnits: PropTypes.number,
      units: PropTypes.number,
      auv: PropTypes.number,
      commentsUnits: PropTypes.number,
      repliesUnits: PropTypes.number,
      threadUrl: PropTypes.string,
    }),
  }).isRequired,
};

Concept.defaultProps = {
};

export default Concept;
