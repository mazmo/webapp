import { useRef, useState } from 'react';
import {
  Alert, AlertIcon, Badge, HStack, Box, Icon,
  AlertDescription, ButtonGroup, Button, IconButton, useDisclosure,
  AlertDialog, AlertDialogOverlay, AlertDialogContent, AlertDialogHeader,
  AlertDialogFooter, AlertDialogBody,
} from '@chakra-ui/react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import { useTranslation } from 'hooks';
import { Coupon } from 'state/bank/types';
import * as bankActions from 'state/bank/actions';

import { TicketPercent, TrashCanOutline } from 'components/Icons';
import useToast from 'hooks/useToast';

import locales from '../i18n';

interface Props {
  coupon: Coupon;
}

const CouponComponent: React.FC<Props> = ({ coupon }) => {
  const { t } = useTranslation(locales);
  const toast = useToast();
  const dispatch = useDispatch();

  const cancelRef = useRef(null);

  const { isOpen, onOpen, onClose } = useDisclosure();
  const [isRemoving, setIsRemoving] = useState(false);

  const remove = async () => {
    try {
      setIsRemoving(true);

      await dispatch(bankActions.removeCoupon(coupon.id));

      setIsRemoving(false);
      toast.success(t('Coupon successfully removed'));
      onClose();
    } catch (error) {
      toast.error(error);
      setIsRemoving(false);
    }
  };

  return (
    <>
      <Alert key={`coupon-${coupon.code}`} status="warning">
        <AlertIcon as={TicketPercent} fill="orange.700" />

        <HStack justifyContent="space-between" w="full">
          <Box>
            <AlertDescription>{coupon.code}</AlertDescription>
            <Badge colorScheme="green" ml={2} variant="solid" mt={-1}>
              {coupon.discount > 0 && <span>{`${coupon.discount}% off`}</span>}
              {!coupon.discount && coupon.increment > 0 && <span>{`+ ${coupon.increment}%`}</span>}
            </Badge>
          </Box>

          <ButtonGroup>
            <Button as={Link} size="xs" variant="primary" to={`/user/sades/buy?coupon=${coupon.code}`}>{t('Use')}</Button>
            <IconButton size="xs" icon={<Icon as={TrashCanOutline} />} aria-label={t('global:Remove')} onClick={onOpen} />
          </ButtonGroup>
        </HStack>
      </Alert>

      {/* Modals */}
      <AlertDialog isOpen={isOpen} onClose={onClose} leastDestructiveRef={cancelRef}>
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader>{t('Remove Coupon')}</AlertDialogHeader>
            <AlertDialogBody>
              {t('Are you sure you want to delete this coupon?')}
            </AlertDialogBody>

            <AlertDialogFooter>
              <ButtonGroup>
                <Button onClick={onClose} ref={cancelRef}>{t('global:Cancel')}</Button>
                <Button variant="danger" onClick={remove} isLoading={isRemoving}>{t('global:Remove')}</Button>
              </ButtonGroup>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  );
};

export default CouponComponent;
