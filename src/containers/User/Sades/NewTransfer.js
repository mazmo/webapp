import { useCallback, useRef } from 'react';
import { Button } from '@chakra-ui/react';

import { useTranslation, useOpenClose } from 'hooks';

import SearchUsersModal from 'components/SearchUsersModal';
import TransferModal from 'components/TransferButton/TransferModal';

import locales from './i18n';

const NewTransfer = () => {
  const { t } = useTranslation(locales);

  const to = useRef(null);
  const [isUserSearchOpen, openUserSearch, closeUserSearch] = useOpenClose();
  const [isTransferOpen, openTransfer, closeTransfer] = useOpenClose();

  const transferTo = useCallback((user) => {
    closeUserSearch();
    to.current = { type: 'USER', id: user };
    openTransfer();
  }, [closeUserSearch, openTransfer]);

  return (
    <>
      <Button onClick={openUserSearch}>{t('New Transfer')}</Button>

      {/* Modals */}
      {isUserSearchOpen && (
        <SearchUsersModal
          title={t('Transfer to')}
          confirm={transferTo}
          close={closeUserSearch}
          confirmLoading={false}
        />
      )}

      {isTransferOpen && (
        <TransferModal to={to.current} close={closeTransfer} />
      )}
    </>
  );
};

NewTransfer.propTypes = {
};

NewTransfer.defaultProps = {
};

export default NewTransfer;
