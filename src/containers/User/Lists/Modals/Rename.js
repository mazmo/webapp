import { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import { useTranslation, useInputValue } from 'hooks';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';
import Input from 'components/Forms/Input';

import locales from '../i18n';

const Rename = ({ listId, close }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const name = useInputValue('');
  const [loading, setLoading] = useState(false);

  const rename = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(authActions.renameFollowList(listId, name.value));
      dispatch(appActions.addToast(t('List renamed')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    close();
    setLoading(false);
  }, [dispatch, t, close, listId, name.value]);

  return (
    <Modal
      title={t('Rename list')}
      onCancel={close}
      actions={[
        <Button key="followlist-rename-confirm" onClick={rename} loading={loading}>{t('global:Confirm')}</Button>,
      ]}
    >
      {t('Enter the new name of the list')}

      <br />
      <br />
      <Input {...name} />
    </Modal>
  );
};

Rename.propTypes = {
  listId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
};

Rename.defaultProps = {
};

export default Rename;
