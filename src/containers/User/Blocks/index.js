import { useCallback, useState } from 'react';
import styled from 'styled-components';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { useTranslation, useTitle } from 'hooks';

import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';

import UnblockModal from 'containers/Profiles/Profile/Modals/Unblock';
import Sidebar from 'containers/Sidebar';
import Layout from 'components/Layout';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import PageTitle from 'components/PageTitle';
import { SelectableList, SelectableListUser } from 'components/SelectableList';
import EmptyState from 'components/EmptyState';
import Button from 'components/Button';
import { TrashCan } from 'components/Icons';

import locales from './i18n';

const Wrapper = styled.div`
  .empty svg {
    width: 24px;
    height: 24px;

    &:hover path {
      fill: ${props => props.theme.colors.main};
    }
  }
`;
Wrapper.displayName = 'Wrapper';

const Lists = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const [selectedUserId, setSelectedUserId] = useState(null);
  useTitle(t('Blocks'));

  const openMiniProfile = useCallback((userId) => (evt) => {
    if (!evt.target.closest('button')) {
      dispatch(appActions.setMiniprofileByUserId(userId));
    }
  }, [dispatch]);

  const blockedUsers = useSelector(authSelectors.selectBlockedUsers, shallowEqual);

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      <FlexWrapper canOverflow>
        <FlexInnerWrapper>
          <PageTitle>{t('Blocks')}</PageTitle>

          <FlexContainer framed>
            <Wrapper>
              <SelectableList>
                {blockedUsers.map(userId => (
                  <SelectableListUser
                    key={userId}
                    userId={userId}
                    // actions={actions}
                    onClick={openMiniProfile(userId)}
                    actions={[<Button key={`userlist-${userId}-remove`} className="empty" onClick={() => { setSelectedUserId(userId); }}><TrashCan color="#A8A8A8" /></Button>]}
                  />
                ))}

                {!blockedUsers.length && (
                  <EmptyState title={t('There are no blocked users yet')} />
                )}
              </SelectableList>
            </Wrapper>
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>
      {selectedUserId && (
        <UnblockModal userId={selectedUserId} close={() => { setSelectedUserId(null); }} />
      )}
      <Sidebar />
    </Layout>
  );
};

Lists.propTypes = {
};

Lists.defaultProps = {
};

export default Lists;
