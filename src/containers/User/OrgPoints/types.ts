export interface User {
  id: string;
  name: string;
  email: string;
  points: number;
}

export interface UpdatePointsData {
  userIds: string[];
  points: number;
  description?: string;
}
