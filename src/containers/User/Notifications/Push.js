import { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userActions from 'state/users/actions';
import * as appActions from 'state/app/actions';

import Toggle from 'components/Toggle';
import Button from 'components/Button';

import locales from './i18n';

const Push = ({ data, token }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [directMessages, setDirectMessages] = useState(data.directMessages);
  const onDirectMessagesChange = useCallback(() => { setDirectMessages(v => !v); }, []);

  const [newFollows, setNewFollows] = useState(data.newFollows);
  const onNewFollowsChange = useCallback(() => { setNewFollows(v => !v); }, []);

  const [newKnowns, setNewKnowns] = useState(data.newKnowns);
  const onNewKnownsChange = useCallback(() => { setNewKnowns(v => !v); }, []);

  const [relationships, setRelationships] = useState(data.relationships);
  const onRelationshipsChange = useCallback(() => { setRelationships(v => !v); }, []);

  const [communityInvites, setCommunityInvites] = useState(data.communityInvites);
  const onCommunityInvitesChange = useCallback(() => { setCommunityInvites(v => !v); }, []);

  const [channelInvites, setChannelInvites] = useState(data.channelInvites);
  const onChannelInvitesChange = useCallback(() => { setChannelInvites(v => !v); }, []);

  const [spanks, setSpanks] = useState(data.spanks);
  const onSpanksChange = useCallback(() => { setSpanks(v => !v); }, []);

  const [newComments, setNewComments] = useState(data.newComments);
  const onNewCommentsChange = useCallback(() => { setNewComments(v => !v); }, []);

  const [chatRequests, setChatRequests] = useState(data.chatRequests);
  const onChatRequestsChange = useCallback(() => { setChatRequests(v => !v); }, []);

  const [chatApproves, setChatApproves] = useState(data.chatApproves);
  const onChatApprovesChange = useCallback(() => { setChatApproves(v => !v); }, []);

  const [mentions, setMentions] = useState(data.mentions);
  const onMentionsChange = useCallback(() => { setMentions(v => !v); }, []);

  const [saving, setSaving] = useState(false);

  const save = useCallback(async () => {
    try {
      setSaving(true);
      await dispatch(userActions.saveNotificationsConfigPush({
        directMessages,
        newFollows,
        newKnowns,
        relationships,
        communityInvites,
        channelInvites,
        spanks,
        newComments,
        chatRequests,
        chatApproves,
        mentions,
      }, token));
      dispatch(appActions.addToast(t('Push notifications saved')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }
    setSaving(false);
  }, [
    directMessages,
    newFollows,
    newKnowns,
    relationships,
    communityInvites,
    channelInvites,
    spanks,
    newComments,
    chatRequests,
    chatApproves,
    mentions,
    dispatch, t, token,
  ]);

  return (
    <div>
      <div style={{ textAlign: 'left' }}>
        <Toggle
          label={t('Private chat messages')}
          onChange={onDirectMessagesChange}
          active={directMessages}
          position="left"
        />

        <br />

        <Toggle
          label={t('Chat requests')}
          onChange={onChatRequestsChange}
          active={chatRequests}
          position="left"
        />

        <br />

        <Toggle
          label={t('Chat approves')}
          onChange={onChatApprovesChange}
          active={chatApproves}
          position="left"
        />

        <br />

        <Toggle
          label={t('New followers')}
          onChange={onNewFollowsChange}
          active={newFollows}
          position="left"
        />

        <br />

        <Toggle
          label={t('New knowns')}
          onChange={onNewKnownsChange}
          active={newKnowns}
          position="left"
        />

        <br />

        <Toggle
          label={t('Relationships')}
          onChange={onRelationshipsChange}
          active={relationships}
          position="left"
        />

        <br />

        <Toggle
          label={t('Community invites')}
          onChange={onCommunityInvitesChange}
          active={communityInvites}
          position="left"
        />

        <br />

        <Toggle
          label={t('Channel invites')}
          onChange={onChannelInvitesChange}
          active={channelInvites}
          position="left"
        />

        <br />

        <Toggle
          label={t('Spanks')}
          onChange={onSpanksChange}
          active={spanks}
          position="left"
        />

        <br />

        <Toggle
          label={t('New comments in your publications')}
          onChange={onNewCommentsChange}
          active={newComments}
          position="left"
        />

        <br />

        <Toggle
          label={t('Mentions')}
          onChange={onMentionsChange}
          active={mentions}
          position="left"
        />

        <br />

      </div>

      <br />
      <br />

      <Button onClick={save} loading={saving}>{t('Save changes')}</Button>
    </div>
  );
};

Push.propTypes = {
  data: PropTypes.shape({
    directMessages: PropTypes.bool.isRequired,
    newFollows: PropTypes.bool.isRequired,
    newKnowns: PropTypes.bool.isRequired,
    relationships: PropTypes.bool.isRequired,
    communityInvites: PropTypes.bool.isRequired,
    channelInvites: PropTypes.bool.isRequired,
    spanks: PropTypes.bool.isRequired,
    newComments: PropTypes.bool.isRequired,
    chatRequests: PropTypes.bool.isRequired,
    chatApproves: PropTypes.bool.isRequired,
    mentions: PropTypes.bool.isRequired,
  }).isRequired,
  token: PropTypes.string,
};

Push.defaultProps = {
  token: null,
};

export default Push;
