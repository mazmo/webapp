import { useCallback } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import * as appSelectors from 'state/app/selectors';
import * as messengerSelectors from 'state/messengers/selectors';
import * as appActions from 'state/app/actions';

import { Menu as IconMenu, Close } from 'components/Icons';

import Badge from './Badge';

const IconMenuWrapper = styled.div`
  display: none;
  @media(max-width: 768px) {
    width: 30px;
    height: 30px;
    display: block;

    ${Badge} {
      right: -10px;
    }
  }
`;

const MenuButton = () => {
  const dispatch = useDispatch();
  const active = useSelector(appSelectors.selectIsNavLeftActive);
  const messengersUnread = useSelector(messengerSelectors.totalUnread);

  const toggle = useCallback(() => {
    dispatch(appActions.toggleNavLeft());
  }, [dispatch]);

  return (
    <IconMenuWrapper onClick={toggle}>
      {!active
        ? <IconMenu color="white" />
        : <Close color="white" />
      }
      {messengersUnread > 0 && <Badge value={messengersUnread} />}
    </IconMenuWrapper>
  );
};

MenuButton.propTypes = {
};

export default MenuButton;
