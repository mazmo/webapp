import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import AlertContainer from '../AlertContainer';
import locales from '../i18n';

const SadesTransaction = ({ timestamp, read }) => {
  const { t } = useTranslation(locales);

  return (
    <AlertContainer timestamp={timestamp} to="/bank" read={read}>
      {t('You have received a new SADEs transaction')}
    </AlertContainer>
  );
};

SadesTransaction.propTypes = {
  read: PropTypes.bool.isRequired,
  timestamp: PropTypes.string.isRequired,
};

export default SadesTransaction;
