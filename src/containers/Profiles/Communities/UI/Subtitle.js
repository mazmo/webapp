import styled from 'styled-components';

const Subtitle = styled.div`
  font-size: 12px;
  color: ${props => props.theme.colors.secondary};
`;
Subtitle.displayName = 'Subtitle';

export default Subtitle;
