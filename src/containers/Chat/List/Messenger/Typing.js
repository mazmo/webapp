import { memo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as messengerSelectors from 'state/messengers/selectors';

import TypingWrapper from '../TypingWrapper';
import locales from '../../i18n';

const Typing = ({ id }) => {
  const { t } = useTranslation(locales);
  const isTyping = useSelector(state => messengerSelectors.getIsTyping(state, id));

  if (!isTyping) return null;

  return (
    <TypingWrapper>{t('typing')}</TypingWrapper>
  );
};

Typing.propTypes = {
  id: PropTypes.string.isRequired,
};

Typing.defaultProps = {
};

export default memo(Typing);
