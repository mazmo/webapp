import styled from 'styled-components';

const TypingWrapper = styled.div`
  color: #666;
  font-weight: normal;
`;

export default TypingWrapper;
