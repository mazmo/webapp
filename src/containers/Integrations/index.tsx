import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, useHistory } from 'react-router-dom';
import { Button } from '@chakra-ui/react';

import useTranslation from 'hooks/useTranslation';
import * as appActions from 'state/app/actions';
import * as appSelectors from 'state/app/selectors';

import { Vibrator, ArrowBack, MercadoPago as MercadoPagoIcon } from 'components/Icons';
import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import { TabsList, TabsWrapper } from 'components/Tabs';
import Layout from 'components/Layout';
import Sidebar from 'containers/Sidebar';
import PageTitle from 'components/PageTitle';

import locales from './i18n';
import Lovense from './Lovense';
import MercadoPago from './MercadoPago';

interface Props {
  match: { path: string };
}

const ICON_COLOR = '#666';

const Integrations: React.FC<Props> = ({ match }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const uiLeftColumn = useSelector(appSelectors.selectIsUiLeftColumnActive);
  const historyCanGoBack = useSelector(appSelectors.selectHistoryCanGoBack);

  const tabs = useMemo(() => {
    return [
      { key: 'mercadopago', label: t('MercadoPago'), icon: <MercadoPagoIcon color={ICON_COLOR} /> },
      { key: 'lovense', label: t('Lovense'), icon: <Vibrator color={ICON_COLOR} /> },
    ];
  }, [t]);

  const onTabChange = useCallback((index: number) => {
    history.replace(`/integrations/${tabs[index].key}`);
  }, [history, tabs]);

  useEffect(() => {
    dispatch(appActions.uiLeftColumn(true));
  }, [uiLeftColumn, dispatch]);

  const onBackClick = useCallback(() => {
    if (historyCanGoBack) {
      history.goBack();
    } else {
      history.push('/');
    }
  }, [history, historyCanGoBack]);

  const tabIndex = tabs.findIndex(tab => document.location.pathname === `${match.path}/${tab.key}`);

  return (
    <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
      {/* @ts-ignore */}
      <FlexWrapper canOverflow>
        <FlexInnerWrapper>
          <Button
            variant="ghost"
            onClick={onBackClick}
            width="36px"
            position="absolute"
            cursor="pointer"
            top="32px"
            left="32px"
            p={0}
          >
            <ArrowBack color="black" />
          </Button>
          <PageTitle>{t('Integrations')}</PageTitle>

          {/* @ts-ignore */}
          <TabsWrapper sticky>
            <TabsList data={tabs} selected={tabIndex} onSelect={onTabChange} />
          </TabsWrapper>

          {/* @ts-ignore */}
          <FlexContainer framed>
            <Route path={`${match.path}/mercadopago`} component={MercadoPago} />
            <Route path={`${match.path}/lovense`} component={Lovense} />
          </FlexContainer>
        </FlexInnerWrapper>
      </FlexWrapper>

      <Sidebar />
    </Layout>
  );
};

export default Integrations;
