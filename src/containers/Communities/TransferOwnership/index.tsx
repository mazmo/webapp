import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation, useTitle } from 'hooks';

import Info from 'containers/Communities/Info';
import NotFound from 'containers/NotFound';
import NotAuthorized from 'containers/Communities/NotAuthorized';
import Layout from 'components/Layout';
import Loading from 'components/Loading';

import * as communitySelectors from 'state/communities/selectors';
import * as membershipSelectors from 'state/memberships/selectors';
import * as communityActions from 'state/communities/actions';

import locales from './i18n';
import Content from './Content';

const TransferOwnership: React.FC = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const { communitySlug } = useParams<{communitySlug: string }>();

  const communityId = useSelector(
    // @ts-ignore
    state => communitySelectors.selectIdBySlug(state, communitySlug),
  );
  const isFetching = useSelector(
    // @ts-ignore
    state => communitySelectors.selectIsFetching(state, communitySlug),
  );
  // @ts-ignore
  const isAdmin = useSelector(state => membershipSelectors.userIsAdminOf(state, communityId));

  useEffect(() => {
    dispatch(communityActions.fetch(communitySlug));
  }, [dispatch, communitySlug]);

  useTitle(t('Transfer Ownership'));

  if (!communityId && !isFetching) return <NotFound />;
  if (isFetching) return <Loading />;
  if (!isAdmin) return <NotAuthorized communityId={communityId} />;

  return (
    <Layout columns={2}>
      <Info communitySlug={communitySlug} />
      <Content communityId={communityId} />
    </Layout>
  );
};

export default TransferOwnership;
