import { useEffect, useState } from 'react';
import { Flex, Spinner } from '@chakra-ui/react';
import { useParams, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import * as eventActions from 'state/events/actions';
import * as appSelectors from 'state/app/selectors';

import EmptyState from 'components/EmptyState';

interface Props {
}

const PayPalCapture: React.FC<Props> = () => {
  const params = useParams<{ eventId: string }>();
  const history = useHistory();
  const dispatch = useDispatch();

  const [error, setError] = useState<string>();

  const areResourcesLoaded = useSelector(appSelectors.selectAreResourcesLoaded);

  useEffect(() => {
    if (areResourcesLoaded) {
      (async () => {
        try {
          const event: any = await dispatch(eventActions.capturePaypalOrder(params.eventId));
          const link = event?.links?.[0];
          if (link) history.push(link.replace('https://mazmo.net', ''));
        } catch (e: any) {
          setError(e.response.data.message);
        }
      })();
    }
  }, [areResourcesLoaded, params.eventId, history, dispatch]);

  if (error) {
    return <EmptyState title="Error" subtitle={error} />;
  }

  return (
    <Flex justifyContent="center" alignItems="center" height="100%">
      <Spinner size="lg" />
    </Flex>
  );
};

export default PayPalCapture;
