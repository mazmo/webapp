import { ChangeEvent, useState } from 'react';
import { Box } from '@chakra-ui/react';

import { useTranslation } from 'hooks';

import ToggleButtonGroup from 'components/ToggleButtonGroup';

import Wrapper from './Wrapper';
import Map from './Map';
import Virtual from './Virtual';
import locales from '../i18n';

interface Props {
  location?: {
    route: string;
    streetNumber: string;
    adminArea: string;
    country: string;
  };
  setLocation: (location: any) => void;
  url: {
    value: string;
    onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  };
  instructions: {
    value: string;
    onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  };
}

const Location: React.FC<Props> = ({
  location, setLocation, url, instructions,
}) => {
  const { t } = useTranslation(locales);
  const [selected, setSelected] = useState('map');

  return (
    <Wrapper>
      <ToggleButtonGroup
        buttons={[
          { key: 'map', label: t('Physical') },
          { key: 'virtual', label: t('Virtual') },
        ]}
        selected={selected}
        onChange={setSelected}
      />

      <Box mt={6}>
        {selected === 'map'
          ? <Map location={location} setLocation={setLocation} instructions={instructions} />
          : <Virtual url={url} instructions={instructions} />
      }
      </Box>
    </Wrapper>
  );
};

export default Location;
