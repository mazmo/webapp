/* eslint-disable max-len */
import {
  Stack, Text, UnorderedList, ListItem,
} from '@chakra-ui/react';

const TOS: React.FC = () => (
  <Stack spacing={4}>
    <Text>
      ¡Te damos la bienvenida a
      {' '}
      <strong>Tus eventos!</strong>
    </Text>

    <Text>
      Como persona organizadora de eventos y participante en ellos quieres que tus eventos se ejecuten de manera segura y sin problemas.
      {' '}
      <strong>Mazmo</strong>
      {' '}
      desea lo mismo y por eso está encantada de que estés aquí.
    </Text>

    <Text>
      Lee atentamente los
      {' '}
      <strong>Términos y Condiciones del Servicio</strong>
      , ya que contienen información importante sobre tus derechos legales, recursos y obligaciones. Al acceder o utilizar
      {' '}
      <strong>Tus Eventos</strong>
      , declaras cumplir y aceptar estos
      {' '}
      <strong>Términos</strong>
      .
    </Text>

    <Text>
      Los puntos más destacables son:
    </Text>

    <UnorderedList spacing={2}>
      <ListItem>
        Aceptas que eres la persona responsable de garantizar que tu evento y cualquier página que muestre contenido sobre el mismo cumplan todas las leyes, normas y reglamentaciones locales, jurisdiccionales, nacionales y otras de aplicación, y que los bienes y servicios descritos en la página del evento se entregan como se describen y de una manera satisfactoria y precisa.
      </ListItem>

      <ListItem>
        Declaras, garantizas y aceptas que
        {' '}
        (a) cumplirás en todo momento todas las leyes, normas y reglamentaciones locales, jurisdiccionales, nacionales y de cualquier otro tipo aplicable con respecto a la información que recopiles (o recibas sobre) consumidores, y
        {' '}
        (b) cumplirás en todo momento las políticas aplicables publicadas en los Servicios con respecto a la información que recopiles de (o recibas sobre) los consumidores.
      </ListItem>

      <ListItem>
        Aceptas brindar indemnidad a
        {' '}
        <strong>Mazmo</strong>
        {' '}
        si utilizas el Servicio de alguna manera por la que
        {' '}
        <strong>Mazmo</strong>
        {' '}
        sea objeto de un asunto legal, o deba afrontar otras reclamaciones o gastos, o según lo establecido aquí, en la medida en que lo permitan las leyes aplicables.
      </ListItem>

      <ListItem>
        Declaras y garantizas que posees todos los derechos, poderes y autoridad necesarios para publicar y disponer de Tu contenido y que el mismo (a) no infringe, incumple, malversa o entra, de cualquier otro modo, en conflicto con los derechos de terceros; (b) cumple todas las leyes, reglas y normativas locales, estatales, jurisdiccionales, nacionales y aplicables de cualquier otro tipo; y (c) no infringe estos
        {' '}
        <strong>Términos</strong>
        .
      </ListItem>
    </UnorderedList>
  </Stack>
);

export default TOS;
