import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import * as communitySelectors from 'state/communities/selectors';

import CommunityAvatar from 'components/CommunityAvatar';

const CommunityWrapper = styled.div`
  display: flex;
  align-items: center;

  img {
    width: 32px;
    height: 32px;
    margin-right: 8px;
  }
`;
CommunityWrapper.displayName = 'CommunityWrapper';

const Community = ({ communityId }) => {
  const name = useSelector(state => communitySelectors.selectName(state, communityId));

  return (
    <CommunityWrapper>
      <CommunityAvatar communityId={communityId} />
      {name}
    </CommunityWrapper>
  );
};

Community.propTypes = {
  communityId: PropTypes.string.isRequired,
};

Community.defaultProps = {
};

export default Community;
