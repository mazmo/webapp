import {
  useCallback, forwardRef, useRef, useImperativeHandle, useState,
} from 'react';
import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import loadImage from 'blueimp-load-image';
import 'react-image-crop/dist/ReactCrop.css';
import { Button } from '@chakra-ui/react';

import { useTranslation } from 'hooks';
import getCroppedImg from 'utils/getCroppedImg';

import FileSelector from 'components/FileSelector';

import UploadButtonWrapper from './UploadButtonWrapper';
import FlyerWrapper from './FlyerWrapper';
import locales from '../i18n';

const Flyer = ({ flyer, setFlyer }, ref) => {
  const { t } = useTranslation(locales);

  const imageDOM = useRef(null);
  const [crop, setCrop] = useState({
    x: 0,
    y: 0,
    width: 187,
    height: 250,
    aspect: 0.75,
  });

  useImperativeHandle(ref, () => ({
    crop: async () => {
      if (imageDOM.current) {
        const croppedImg = await getCroppedImg(imageDOM.current, crop, 'temp.jpg');
        return croppedImg;
      }
      return null;
    },
  }));

  const onFlyerChange = useCallback((e) => {
    loadImage(
      e.target.files[0],
      (img) => {
        const base64data = img.toDataURL('image/jpeg');
        setFlyer({
          src: base64data,
          img,
        });
      },
      {
        maxWidth: 600, maxHeight: 700, orientation: true, canvas: true,
      },
    );
  }, [setFlyer]);

  const imageLoaded = useCallback((img) => {
    imageDOM.current = img;
  }, [imageDOM]);
  const onCropChange = useCallback((data) => {
    setCrop(data);
  }, [setCrop]);

  return (
    <FlyerWrapper>
      <h3>{t('Flyer')}</h3>

      {!flyer
        ? (
          <UploadButtonWrapper>
            <FileSelector onChange={onFlyerChange}>
              <Button size="lg">{t('Upload image')}</Button>
            </FileSelector>
          </UploadButtonWrapper>
        ) : (
          <div className="flyer">
            <ReactCrop
              src={flyer.src}
              onChange={onCropChange}
              onImageLoaded={imageLoaded}
              crop={crop}
              minWidth={100}
              minHeight={100}
              keepSelection
            />
          </div>
        )}
    </FlyerWrapper>
  );
};

Flyer.propTypes = {
  flyer: PropTypes.shape({
    src: PropTypes.string,
  }),
  setFlyer: PropTypes.func.isRequired,
};

Flyer.defaultProps = {
  flyer: null,
};

export default forwardRef(Flyer);
