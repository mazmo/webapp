import styled from 'styled-components';

const FlyerWrapper = styled.div`
  h3 {
    color: #666;
    margin-bottom: 8px;
    font-weight: 300;
  }

  .flyer {
    text-align: center;
  }
`;
FlyerWrapper.displayName = 'FlyerWrapper';

export default FlyerWrapper;
