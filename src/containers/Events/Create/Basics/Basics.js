import {
  useState, useCallback, useRef, useEffect,
} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Accordion, AccordionButton, AccordionIcon, AccordionItem, AccordionPanel, Box, Select, Text, Button as ChakraButton,
} from '@chakra-ui/react';

import { useTranslation, useOpenClose, useInputValue } from 'hooks';

import Input from 'components/Forms/Input';
import Toggle from 'components/Toggle';
import Datepicker from 'components/Datepicker';
import Button from 'components/Button';
import Modal from 'components/Modal';

import Wrapper from './Wrapper';
import locales from '../i18n';

const Basics = ({
  isPublic, setIsPublic, showLocation, setShowLocation, date,
  setDate, name, time, price, currency, sponsors, setSponsors,
}) => {
  const { t } = useTranslation(locales);
  const nameElement = useRef(null);

  const [isFocused, setIsFocused] = useState(false);
  const [isopenAddSponsor, openAddSponsor, closeAddSponsor] = useOpenClose(false);
  const sponsorName = useInputValue('');
  const sponsorLink = useInputValue('');

  useEffect(() => {
    if (nameElement.current) {
      nameElement.current.focus();
    }
  }, []);

  useEffect(() => {
    if (!isopenAddSponsor && (sponsorName.value.length || sponsorLink.value.length)) {
      sponsorName.change('');
      sponsorLink.change('');
    }
  }, [isopenAddSponsor, sponsorName, sponsorLink]);

  const onFocusChange = useCallback(({ focused }) => setIsFocused(focused), []);
  const isOutsideRange = useCallback(d => moment().diff(d, 'days') > 0, []);
  const initialVisibleMonth = useCallback(() => moment().add(7, 'days'), []);

  const togglePublic = useCallback((e) => {
    setIsPublic(e.target.checked);
  }, [setIsPublic]);

  const toggleShowLocation = useCallback((e) => {
    setShowLocation(!e.target.checked);
  }, [setShowLocation]);

  const addSponsor = useCallback(() => {
    setSponsors(cv => [
      ...cv,
      { name: sponsorName.value, link: sponsorLink.value },
    ]);

    closeAddSponsor();
  }, [setSponsors, sponsorName.value, sponsorLink.value, closeAddSponsor]);

  const removeSponsor = useCallback(index => () => {
    setSponsors(cv => [
      ...cv.slice(0, index),
      ...cv.slice(index + 1),
    ]);
  }, [setSponsors]);

  return (
    <>
      <Wrapper>
        <h3>{t('Event name')}</h3>
        <Input {...name} ref={nameElement} />

        <h3>{t('Date and Time')}</h3>
        <div className="datetime">
          <Datepicker
            id="create-event-date"
            date={date}
            onDateChange={setDate}
            focused={isFocused}
            onFocusChange={onFocusChange}
            required
            keepOpenOnDateSelect={false}
            numberOfMonths={1}
            showClearDate
            block
            hideKeyboardShortcutsPanel
            isOutsideRange={isOutsideRange}
            initialVisibleMonth={initialVisibleMonth}
            monthSelection={false}
          />

          <input type="time" {...time} className="timepicker" />
        </div>

        <h3>{t('Ticket Price')}</h3>
        <div className="price">
          <Input type="number" {...price} />
          <Select w="100px" {...currency}>
            <option value="EUR">EUR</option>
            <option value="USD">USD</option>
            <option value="ARS">ARS</option>
          </Select>
        </div>

        <h3>{t('Options')}</h3>
        <div className="options">
          <Toggle label={t('The Event is in a public space (bar, cafe, etc.)')} active={isPublic} onChange={togglePublic} position="left" />

          <Toggle label={t('Show location after RSVP')} active={!showLocation} onChange={toggleShowLocation} position="left" />
        </div>

        <Accordion allowToggle>
          <AccordionItem>
            <h2>
              <AccordionButton>
                <Box as="span" flex="1" textAlign="left">
                  {t('Sponsors')}
                </Box>
                <AccordionIcon />
              </AccordionButton>
            </h2>
            <AccordionPanel pb={4}>
              <div className="sponsors">
                {sponsors.length === 0 && <Text w="full" align="center" bgColor="gray.50" color="gray.400" py={8}>{t('No sponsors yet')}</Text>}
                {sponsors.map((sponsor, index) => (
                  <div className="sponsor" key={`sponsor-${sponsor.name}`}>
                    <strong>{sponsor.name}</strong>
                    {': '}
                    <span>{sponsor.link}</span>
                    <ChakraButton variant="outline" size="xs" onClick={removeSponsor(index)}>{t('global:Remove')}</ChakraButton>
                  </div>
                ))}
                <ChakraButton size="xs" variant="outline" mt={1} onClick={openAddSponsor}>{t('global:Add')}</ChakraButton>
              </div>
            </AccordionPanel>
          </AccordionItem>
        </Accordion>
      </Wrapper>

      {/* Modals */}
      {isopenAddSponsor && (
        <Modal
          title={t('Add sponsor')}
          onCancel={closeAddSponsor}
          actions={[
            <Button key="add-sponsor" onClick={addSponsor}>{t('global:Add')}</Button>,
          ]}
        >
          <Input {...sponsorName} placeholder={t('Name')} />
          <Input {...sponsorLink} placeholder={t('Link')} />
        </Modal>
      )}
    </>
  );
};

Basics.propTypes = {
  isPublic: PropTypes.bool.isRequired,
  setIsPublic: PropTypes.func.isRequired,
  showLocation: PropTypes.bool.isRequired,
  setShowLocation: PropTypes.func.isRequired,
  date: PropTypes.instanceOf(moment).isRequired,
  setDate: PropTypes.func.isRequired,
  name: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  time: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  price: PropTypes.shape({
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  currency: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  sponsors: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
  })).isRequired,
  setSponsors: PropTypes.func.isRequired,
};

Basics.defaultProps = {
};

export default Basics;
