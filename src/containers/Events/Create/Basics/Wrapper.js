import styled from 'styled-components';

const Wrapper = styled.div`
  h3 {
    color: #666;
    margin-bottom: 8px;
    font-weight: 300;
  }

  .datetime {
    display: flex;

    > div:first-child {
      margin-right: 40px;
    }

    .DateInput_input {
      font-weight: 500;
      line-height: 56px;
      border: 1px solid #F5F0F0;
      box-shadow: 0px 2px 4px rgba(0,0,0,0.04), 0px 4px 8px rgba(0,0,0,0.08);
      border-radius: 8px;
    }

    .SingleDatePickerInput {
      border: 0;
    }

    .DateInput_input {
      border: 0;
      padding-left: 16px;
    }

    .timepicker {
      font-weight: 500;
      border: 1px solid #F5F0F0;
      box-shadow: 0px 2px 4px rgba(0,0,0,0.04), 0px 4px 8px rgba(0,0,0,0.08);
      border-radius: 8px;
      padding: 0 16px;
      line-height: 16px;
      height: 56px;
      font-family: unset;
      font-weight: normal;
      font-size: 16px;
    }
  }

  .price {
    display: flex;
    align-items: baseline;

    input {
      width: fit-content;
      margin-right: 16px;
    }
  }

  .options {
    margin-bottom: 32px;

    > label {
      margin-bottom: 8px;
    }
  }

  .sponsors {
    .sponsor {
      button {
        margin-left: 24px;
      }
    }
  }
`;
Wrapper.displayName = 'Wrapper';

export default Wrapper;
