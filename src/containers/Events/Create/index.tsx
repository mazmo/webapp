import {
  useEffect, useState, useCallback, useRef,
} from 'react';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import {
  Box, Button, Text, HStack, Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, useDisclosure,
  Card,
  CardBody,
  CardFooter,
  ButtonGroup,
  CardHeader,
} from '@chakra-ui/react';

import ComposerRef from 'utils/ComposerRef';
import {
  useTitle, useTranslation, useInputValue,
} from 'hooks';
import * as appSelectors from 'state/app/selectors';
import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';
import * as eventActions from 'state/events/actions';

import PageHeader from 'components/PageHeader';

import TOS from './TOS';
import Basics from './Basics';
import Flyer from './Flyer';
import LocationStep from './Location';
import Description from './Description';
import Features from './Features';
import WaitingThread from './WaitingThread';
import locales from './i18n';

export type FlyerHandle = {
  crop: () => Promise<string>;
};

type Location = {
  streetNumber: string;
  route: string;
  adminArea: string;
  country: string;
};

export type EventPayload = {
  name: string;
  date: moment.Moment;
  description: string;
  communityId: string;
  instructions: string;
  cover: string;
  organizers: { userId: string }[];
  price: string;
  currency: string;
  isPublic: boolean;
  showLocation: boolean;
  features: {
    highlight?: boolean;
    assistance?: boolean;
    transactional?: boolean;
    enrollments?: boolean;
    earlyTickets?: boolean;
  };
  sponsors: {
    name: string;
    link: string;
  }[];
  address?: Location;
  coordinates?: [number, number];
  url?: string;
};

const CreatePage: React.FC = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  useTitle(t('Create Event'));

  const flyerElement = useRef<FlyerHandle>();
  const composerId = 'eventcreate';

  const [step, setStep] = useState(0);
  const [creating, setCreating] = useState(false);
  const { isOpen: openedHLUnactive, onOpen: openHLUnactive, onClose: closeHLUnactive } = useDisclosure();

  const descriptionReady = useSelector(
    // @ts-ignore
    state => appSelectors.selectIsComposerReadyToSubmit(state, composerId),
  );
  const userId = useSelector(authSelectors.selectUserId);
  const userCountry = useSelector(authSelectors.selectCountry);
  const defaultCurrency = userCountry?.isoCode === 'AR' ? 'ARS' : 'EUR';
  const defaultPrice = userCountry?.isoCode === 'AR' ? 10000 : 50;

  // Values
  const [isPublic, setIsPublic] = useState(false);
  const [showLocation, setShowLocation] = useState(true);
  const name = useInputValue('');
  const [date, setDate] = useState(moment().add(7, 'days'));
  const { ...time } = useInputValue('23:00');
  const price = useInputValue(defaultPrice.toString());
  const currency = useInputValue(defaultCurrency);
  const [sponsors, setSponsors] = useState([]);

  const [flyer, setFlyer] = useState(null);
  const [croppedFlyer, setCroppedFlyer] = useState<string>();

  const instructions = useInputValue('');
  const url = useInputValue('');
  const [location, setLocation] = useState<Location & { lat: number; lng: number }>();

  const [communityId, setCommunityId] = useState<string>();

  const [featureHighlight, setFeatureHighlight] = useState(false);
  const [featureAssistance, setFeatureAssistance] = useState(false);
  const [featureTransactional, setFeatureTransactional] = useState(false);
  const [featureEarly, setFeatureEarly] = useState(false);
  const [featurePreenrollment, setFeaturePreenrollment] = useState(false);
  //

  const [eventId, setEventId] = useState<string>();

  useEffect(() => {
    dispatch(appActions.uiLeftColumn(true));
  }, [dispatch]);

  const addStep = useCallback((q: number) => async () => {
    if (step === 2 && flyerElement.current) {
      const croppedImg = await flyerElement.current.crop();
      setCroppedFlyer(croppedImg);
    }

    if (step === 5 && (featureAssistance || featureTransactional) && !featureHighlight) {
      openHLUnactive();
    } else {
      setStep(currentValue => currentValue + q);
    }
  }, [step, featureAssistance, featureTransactional, featureHighlight, openHLUnactive]);

  const create = async () => {
    try {
      if ((featureAssistance || featureTransactional) && !featureHighlight) {
        openHLUnactive();
      } else {
        setCreating(true);

        if (!communityId) {
          throw new Error(t('Community is required'));
        }

        const uploadedFlyer: any = await dispatch(eventActions.uploadFlyer(croppedFlyer));
        const composerRef = ComposerRef.getRef(composerId);
        const description = composerRef.getValue();

        const eventTime = moment(time.value, 'HH:mm');
        const eventDate = moment(date);
        eventDate.set({
          hour: eventTime.get('hour'),
          minute: eventTime.get('minute'),
          seconds: 0,
        });

        const payload: EventPayload = {
          name: name.value,
          date: eventDate,
          description,
          communityId,
          instructions: instructions.value,
          cover: uploadedFlyer.filename,
          organizers: [{ userId }],
          price: price.value,
          currency: currency.value,
          isPublic,
          showLocation,
          features: {},
          sponsors,
        };

        if (location) {
          payload.address = {
            streetNumber: location.streetNumber,
            route: location.route,
            adminArea: location.adminArea,
            country: location.country,
          };
          payload.coordinates = [location.lat, location.lng];
        } else if (url.value) {
          payload.url = url.value;
        }

        if (featureHighlight) {
          payload.features.highlight = true;
        }
        if (featureAssistance) {
          payload.features.assistance = true;

          if (featurePreenrollment) {
            payload.features.enrollments = true;
          }
        }
        if (featureTransactional) {
          payload.features.transactional = true;
        }
        if (featureEarly) {
          payload.features.earlyTickets = true;
        }

        const event: any = await dispatch(eventActions.create(payload));

        setEventId(event.id);
        setStep(currentValue => currentValue + 1);
      }
    } catch (error) {
      setCreating(false);
      dispatch(appActions.addError(error));
    }
  };

  const nextDisabled = (
    (step === 1 && name.value.length === 0)
    || (step === 2 && !flyer)
    || (step === 3 && !location && !url.value)
    || (step === 4 && (!communityId || !descriptionReady))
  );

  return (
    <>
      <Box as="main" p={4} bgColor="gray.50" minH="calc(100vh - 64px)" mt={{ base: '64px', md: 0 }}>
        <Card w={{ base: 'full', md: '800px' }} mx="auto">
          <CardHeader>
            <PageHeader mb={0} title={t('Create Event')}>
              {step < 5 && <Text color="gray.500">{t('Step {{step}}/{{total}}', { step: step + 1, total: 5 })}</Text>}
            </PageHeader>
          </CardHeader>
          <CardBody>
            {step === 0 && <TOS />}
            {step === 1 && (
            <Basics
              isPublic={isPublic}
              setIsPublic={setIsPublic}
              showLocation={showLocation}
              setShowLocation={setShowLocation}
              date={date}
              setDate={setDate}
              name={name}
              time={time}
              price={price}
              currency={currency}
              sponsors={sponsors}
              setSponsors={setSponsors}
            />
            )}
            {step === 2 && (
            <Flyer
              ref={flyerElement}
              flyer={flyer}
              setFlyer={setFlyer}
            />
            )}
            {step === 3 && (
            <LocationStep
              location={location}
              setLocation={setLocation}
              url={url}
              instructions={instructions}
            />
            )}
            {step === 4 && (
            <Description
              composerId={composerId}
              communityId={communityId}
              setCommunityId={setCommunityId}
            />
            )}
            {step === 5 && (
            <Features
              price={Number(price.value)}
              currency={currency.value as 'ARS' | 'EUR' | 'USD'}
              highlight={featureHighlight}
              setHighlight={setFeatureHighlight}
              assistance={featureAssistance}
              setAssistance={setFeatureAssistance}
              transactional={featureTransactional}
              setTransactional={setFeatureTransactional}
              early={featureEarly}
              setEarly={setFeatureEarly}
              preenrollment={featurePreenrollment}
              setPreenrollment={setFeaturePreenrollment}
            />
            )}
            {step === 6 && eventId && <WaitingThread eventId={eventId} />}
          </CardBody>

          {step < 6 && (
          <CardFooter>
            <ButtonGroup justifyContent="space-between" w="full" size={{ base: 'sm', md: 'md' }}>
              <HStack>
                <Button as="a" href="/events">{t('global:Cancel')}</Button>
                {step > 0 && <Button onClick={addStep(-1)}>{t('Previous')}</Button>}
              </HStack>
              {step === 0 && <Button onClick={addStep(1)} isDisabled={nextDisabled} variant="primary">{t('global:Accept')}</Button>}
              {step > 0 && step < 5 && <Button onClick={addStep(1)} isDisabled={nextDisabled} variant="primary">{t('Next')}</Button>}
              {step === 5 && <Button onClick={create} isDisabled={nextDisabled} isLoading={creating} variant="primary">{t('Accept and Create')}</Button>}
            </ButtonGroup>
          </CardFooter>
          )}
        </Card>
      </Box>

      {/* Modals */}
      {openedHLUnactive && (
        <Modal isOpen={openedHLUnactive} onClose={closeHLUnactive}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>{t('Premium options')}</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              {t('Highlight option is required in order to activate any other premium option.')}
            </ModalBody>
          </ModalContent>
        </Modal>
      )}
    </>
  );
};

export default CreatePage;
