import { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { Trans } from 'react-i18next';
import { Link } from 'react-router-dom';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import {
  VStack,
  Box,
  Text,
  Heading,
  Switch,
  FormControl,
  FormLabel,
  Alert,
  AlertIcon,
  Button,
  HStack,
  List,
  ListItem,
} from '@chakra-ui/react';

import locales from '../i18n';

const ars = new Intl.NumberFormat('es-AR', {
  style: 'currency',
  currency: 'ARS',
  currencyDisplay: 'code',
  maximumFractionDigits: 0,
});
const eur = new Intl.NumberFormat('es-ES', {
  style: 'currency',
  currency: 'EUR',
});
const usd = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

interface FeaturesProps {
  price: number;
  currency: 'ARS' | 'EUR' | 'USD';
  assistance: boolean;
  setAssistance: (value: boolean) => void;
  highlight: boolean;
  setHighlight: (value: boolean) => void;
  transactional: boolean;
  setTransactional: (value: boolean) => void;
  early: boolean;
  setEarly: (value: boolean) => void;
  preenrollment: boolean;
  setPreenrollment: (value: boolean) => void;
}

const Features: React.FC<FeaturesProps> = ({
  price, currency, assistance, setAssistance, highlight, setHighlight,
  transactional, setTransactional, early, setEarly, preenrollment, setPreenrollment,
}) => {
  const { t } = useTranslation(locales);

  const publicKey = useSelector(authSelectors.getMercadoPagoPublicKey);

  const assistanceChanged = useCallback((v: React.ChangeEvent<HTMLInputElement>) => {
    const isChecked = v.target.checked;
    setAssistance(isChecked);
    if (!isChecked) setPreenrollment(false);
  }, [setAssistance, setPreenrollment]);

  const highlightChanged = useCallback((v: React.ChangeEvent<HTMLInputElement>) => {
    const isChecked = v.target.checked;
    setHighlight(isChecked);
    if (!isChecked) {
      setAssistance(false);
      setPreenrollment(false);
    }
  }, [setHighlight, setAssistance, setPreenrollment]);

  const transactionalChanged = useCallback((v: React.ChangeEvent<HTMLInputElement>) => {
    setTransactional(v.target.checked);
  }, [setTransactional]);

  const earlyChanged = useCallback((v: React.ChangeEvent<HTMLInputElement>) => {
    setEarly(v.target.checked);
  }, [setEarly]);

  const preenrollmentChanged = useCallback((v: React.ChangeEvent<HTMLInputElement>) => {
    setPreenrollment(v.target.checked);
  }, [setPreenrollment]);

  const currencyFormatter = {
    ARS: ars.format,
    EUR: eur.format,
    USD: usd.format,
  }[currency];

  const prices = {
    ARS: { highlight: 3500, assistance: 18900, transactional: 7000 },
    EUR: { highlight: 5, assistance: 25, transactional: 20 },
    USD: { highlight: 5, assistance: 25, transactional: 20 },
  }[currency];

  return (
    <Box p={4} bg="white" borderRadius="md" boxShadow="sm">
      <Heading as="h3" size="md" mb={4}>{t('Premium options')}</Heading>

      <VStack spacing={6} align="stretch">
        <Box>
          <FormControl display="flex" alignItems="center">
            <Switch
              id="highlight"
              isChecked={highlight}
              onChange={highlightChanged}
              mr={3}
            />
            <FormLabel htmlFor="highlight" mb={0}>{t('Highlight')}</FormLabel>
          </FormControl>
          <Box ml={12}>
            <Text mt={2}>{t('highlight.details')}</Text>
            <Text fontWeight="bold" mt={4}>
              {t('highlight.pricing', { highlightPrice: currencyFormatter(prices.highlight) })}
            </Text>
          </Box>
        </Box>

        <Box>
          <FormControl display="flex" alignItems="center">
            <Switch
              id="assistance"
              isChecked={assistance}
              onChange={assistanceChanged}
              isDisabled={!highlight}
              mr={3}
            />
            <FormLabel htmlFor="assistance" mb={0}>{t('Assistance tracking')}</FormLabel>
          </FormControl>
          <Box ml={12} fontSize="sm">
            <List mt={2}>
              <ListItem>{t('assistance.details.managing')}</ListItem>
              <ListItem>{t('assistance.details.profile')}</ListItem>
              <ListItem>{t('assistance.details.events')}</ListItem>
              <ListItem>{t('assistance.details.telegram')}</ListItem>
            </List>
            <Text fontWeight="bold" mt={4}>
              {t('assistance.pricing', { assistancePrice: currencyFormatter(prices.assistance) })}
            </Text>
          </Box>
        </Box>

        <Box>
          <FormControl display="flex" alignItems="center">
            <Switch
              id="preenrollment"
              isChecked={preenrollment}
              onChange={preenrollmentChanged}
              isDisabled={!assistance}
              mr={3}
            />
            <FormLabel htmlFor="preenrollment" mb={0}>{t('Pre enrollment')}</FormLabel>
          </FormControl>
          <Box ml={12} fontSize="sm">
            <Text mt={2}>{t('preenrollment.details')}</Text>
            <Text fontWeight="bold" mt={4}>{t('preenrollment.pricing')}</Text>
          </Box>
        </Box>

        <Box>
          <FormControl display="flex" alignItems="center">
            <Switch
              id="transactional"
              isChecked={transactional}
              onChange={transactionalChanged}
              mr={3}
            />
            <FormLabel htmlFor="transactional" mb={0}>{t('Transactional e-mails')}</FormLabel>
          </FormControl>
          <Box ml={12} fontSize="sm">
            <Text mt={2}>{t('transactional.details')}</Text>
            <Text fontWeight="bold" mt={4}>
              {t('transactional.pricing', { transactionalPrice: currencyFormatter(prices.transactional) })}
            </Text>
          </Box>
        </Box>

        <Box>
          <FormControl display="flex" alignItems="center">
            <Switch
              id="early"
              isChecked={early}
              onChange={earlyChanged}
              isDisabled={!publicKey || currency !== 'ARS'}
              mr={3}
            />
            <FormLabel htmlFor="early" mb={0}>{t('Early tickets')}</FormLabel>
          </FormControl>
          {!publicKey && (
            <Alert status="warning" mt={2} flexDirection="column" fontSize="sm">
              <HStack mb={2}>
                <AlertIcon m={0} />
                <Text as="strong">{t('Warning!')}</Text>
              </HStack>
              <Trans t={t} i18nKey="early.details.warning" ns="EventCreate">
                {'This options is disabled because you didn\'t set your payment integration'}
                <Button as={Link} to="/integrations/mercadopago" mt={4}>Set up</Button>
              </Trans>
            </Alert>
          )}
          <Box ml={12} fontSize="sm">
            <Text mt={2}>{t('early.details')}</Text>
            <Text fontWeight="bold" mt={4}>
              {t('early.pricing', { earlyTicketsCommission: currencyFormatter(price * 0.1) })}
            </Text>
          </Box>
        </Box>
      </VStack>
    </Box>
  );
};

export default Features;
