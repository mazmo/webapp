import { useSelector } from 'react-redux';
import { HStack, Stack, Text } from '@chakra-ui/react';

import * as userSelectors from 'state/users/selectors';

import UserLink from 'components/UserLink';
import UserDisplayName from 'components/UserDisplayName';
import UserAvatar from 'components/UserAvatar';

interface Props {
  userId: number;
}

const User: React.FC<Props> = ({ userId }) => {
  const username = useSelector(userSelectors.getUsername(userId));

  return (
    <UserLink userId={userId}>
      <HStack className="userrow">
        <UserAvatar userId={userId} size="36px" />
        <Stack spacing={0} lineHeight={1}>
          <UserDisplayName userId={userId} />
          <Text color="gray.400" fontSize="sm">{`@${username}`}</Text>
        </Stack>
      </HStack>
    </UserLink>
  );
};

export default User;
