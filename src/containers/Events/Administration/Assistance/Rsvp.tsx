import { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';
import * as eventActions from 'state/events/actions';
import * as appActions from 'state/app/actions';

import {
  Tr, Td, Button, Text, VStack, Hide, Show, Box,
  Checkbox,
} from '@chakra-ui/react';

import User from './User';
import locales from '../i18n';

interface RsvpProps {
  rsvp: {
    id: string;
    userId: number;
    event: string;
    assistedAt: string;
    verificationCode: number;
  };
  filter: string;
  isSelected: boolean;
  toggleSelect?: (userId: number) => void;
}

const Rsvp: React.FC<RsvpProps> = ({
  rsvp, filter, isSelected, toggleSelect,
}) => {
  const { t } = useTranslation(locales);
  const { id: rsvpId, event: eventId, userId } = rsvp;

  const username = useSelector(userSelectors.getUsername(userId));
  const displayname = useSelector(userSelectors.getDisplayName(userId));

  const dispatch = useDispatch();
  const [loading, setLoading] = useState<number[]>([]);

  const assist = useCallback(async () => {
    try {
      setLoading((cv) => [...cv, userId]);
      await dispatch(eventActions.adminAssist(eventId, userId));
    } catch (error) {
      dispatch(appActions.addError(error));
    }
    setLoading(cv => cv.filter(id => id !== userId));
  }, [dispatch, eventId, userId]);

  const unassist = useCallback(async () => {
    try {
      setLoading(cv => [...cv, userId]);
      await dispatch(eventActions.adminUnassist(eventId, userId));
    } catch (error) {
      dispatch(appActions.addError(error));
    }
    setLoading(cv => cv.filter(id => id !== userId));
  }, [dispatch, eventId, userId]);

  if (!username.toLowerCase().includes(filter) && !displayname.toLowerCase().includes(filter)) return null;

  return (
    <Tr key={`assistance-${rsvpId}`}>
      {toggleSelect && (
        <Td width="64px">
          <Checkbox isChecked={isSelected} onChange={() => toggleSelect(rsvp.userId)} />
        </Td>
      )}
      <Hide below="md">
        <Td color="gray.500" width="15%">
          {rsvp.verificationCode}
        </Td>
      </Hide>
      <Td width={{ base: '70%', md: '60%' }} className={!rsvp.assistedAt ? 'unassisted' : ''}>
        <VStack align="start" spacing={1} width="100%">
          <Box width="100%" overflow="hidden" textOverflow="ellipsis" whiteSpace="nowrap">
            <User userId={rsvp.userId} />
          </Box>
          <Show below="md">
            <Text fontSize="xs" color="gray.500">
              {t('Order #')}
              :
              {rsvp.verificationCode}
            </Text>
          </Show>
        </VStack>
      </Td>
      <Td width={{ base: '30%', md: '25%' }}>
        <Button
          variant="outline"
          isLoading={loading.includes(rsvp.userId)}
          onClick={rsvp.assistedAt ? unassist : assist}
          size="xs"
          width={{ base: 'full', md: 'auto' }}
        >
          {rsvp.assistedAt ? t('Remove assistance') : t('Add assistance')}
        </Button>
      </Td>
    </Tr>
  );
};

export default Rsvp;
