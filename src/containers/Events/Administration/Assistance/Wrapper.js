import styled from 'styled-components';

const Wrapper = styled.div`
  .counter {
    display: flex;
    width: 100%;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 32px;

    input {
      max-width: 300px;
      border-bottom: 1px solid #EEE;
    }
  }
`;
Wrapper.displayName = 'Wrapper';

export default Wrapper;
