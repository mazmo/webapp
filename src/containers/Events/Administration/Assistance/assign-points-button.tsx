import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as userSelectors from 'state/users/selectors';

import { UpdatePointsModal } from 'containers/User/OrgPoints/update-points-modal';
import { User } from 'components/user-search-input';

import locales from '../i18n';

interface AssignPointsButtonProps {
  pointsName: string;
  selectedUserIds: number[];
}

export const AssignPointsButton: React.FC<AssignPointsButtonProps> = ({ pointsName, selectedUserIds }) => {
  const { t } = useTranslation(locales);

  const users = useSelector(userSelectors.getBatchByIds(selectedUserIds));
  const selectedUsers: User[] = Object.values(users).map((user) => ({
    id: user.id,
    fullId: user.fullId,
    username: user.username,
    displayname: user.displayname,
    avatar: user.avatar,
  }));

  return (
    <UpdatePointsModal
      buttonText={t('Assign {{points}}', { points: pointsName })}
      buttonProps={{
        size: 'sm',
        mb: 4,
        isDisabled: selectedUserIds.length === 0,
      }}
      fixedSelectedUsers={selectedUsers}
    />
  );
};
