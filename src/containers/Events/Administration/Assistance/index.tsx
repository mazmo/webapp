import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import {
  Table, Thead, Tbody, Tr, Th, Box, Hide,
  Text,
  HStack,
  Input,
} from '@chakra-ui/react';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';
import * as userSelectors from 'state/users/selectors';
import * as userActions from 'state/users/actions';

import Rsvp from './Rsvp';
import locales from '../i18n';
import { AssignPointsButton } from './assign-points-button';

const Assistance = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const { eventId } = useParams<{ eventId: string }>();

  const rsvps = useSelector(
    // @ts-ignore
    state => eventSelectors.selectRsvpsSorted(state, eventId),
    shallowEqual,
  );
  const organizers = useSelector(
    // @ts-ignore
    state => eventSelectors.selectOrganizerIds(state, eventId),
    shallowEqual,
  );
  const organizerUserId = organizers[0];

  const orgPointsConfig = useSelector(userSelectors.getOrgPoints(organizerUserId));
  const hasOrgPoints = !!orgPointsConfig?.enabled;

  const [filter, setFilter] = useState('');
  const [selectedUsers, setSelectedUsers] = useState<number[]>([]);
  const toggleSelectUser = (userId: number) => {
    setSelectedUsers((prev) => {
      if (prev.includes(userId)) {
        return prev.filter((id) => id !== userId);
      }
      return [...prev, userId];
    });
  };

  useEffect(() => {
    dispatch(userActions.loadFromIds([organizerUserId]));
  }, [dispatch, organizerUserId]);

  return (
    <Box>
      <HStack mb={4} flexDirection={{ base: 'column', md: 'row' }} alignItems={{ base: 'stretch', md: 'center' }} gap={2}>
        <Input value={filter} onChange={(e: any) => setFilter(e.target.value.toLowerCase())} placeholder={t('global:Filter')} w={{ base: 'full', md: '50%' }} />
        <Text color="gray.500" textAlign="right" mb={{ base: 2, md: 0 }} noOfLines={1} flexGrow={1}>{t('Assistants {{count}}', { count: rsvps.filter((r: any) => !!r.assistedAt).length })}</Text>
      </HStack>

      {hasOrgPoints && (
        <AssignPointsButton pointsName={orgPointsConfig.pluralName} selectedUserIds={selectedUsers} />
      )}

      <Table variant="striped" size={{ base: 'sm', md: 'md' }} width="100%">
        <Thead>
          <Tr>
            {hasOrgPoints && (
              <Th width="64px" />
            )}
            <Hide below="md">
              <Th whiteSpace="nowrap" width="15%">{t('Order #')}</Th>
            </Hide>
            <Th width={{ base: '70%', md: '60%' }}>{t('User')}</Th>
            <Th width={{ base: '30%', md: '25%' }} isNumeric>{t('Action')}</Th>
          </Tr>
        </Thead>
        <Tbody>
          {rsvps.map((rsvp: any) => (
            <Rsvp
              key={`assistance-${rsvp.id}`}
              rsvp={rsvp}
              filter={filter}
              isSelected={selectedUsers.includes(rsvp.userId)}
              toggleSelect={toggleSelectUser}
            />
          ))}
        </Tbody>
      </Table>
    </Box>
  );
};

export default Assistance;
