import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import {
  Card, CardHeader, CardBody, Box,
} from '@chakra-ui/react';

import * as appActions from 'state/app/actions';

import Header from './Header';
import Content from './Content';

const Administration: React.FC = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(appActions.uiLeftColumn(true));
  }, [dispatch]);

  return (
    <Box as="main" p={4} bgColor="gray.50" minH="calc(100vh - 64px)" mt={{ base: '64px', md: 0 }}>
      <Card w={{ base: 'full', md: '800px' }} mx="auto">
        <CardHeader>
          <Header />
        </CardHeader>
        <CardBody>
          <Content />
        </CardBody>
      </Card>
    </Box>
  );
};

export default Administration;
