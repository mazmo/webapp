import { useMemo, useCallback } from 'react';
import { useParams, useHistory, useRouteMatch } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Box, Icon } from '@chakra-ui/react';
import {
  Pencil, UserCheck, Mail, Ban,
  UserPlus,
} from 'lucide-react';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';

import { TabsList, TabsWrapper } from 'components/Tabs';
import PageHeader from 'components/PageHeader';

import locales from './i18n';

const Header = () => {
  const { t } = useTranslation(locales);
  const history = useHistory();
  const { eventId } = useParams<{ eventId: string }>();
  const match = useRouteMatch<{ key: string }>('/events/:eventId/:key');

  const isLoaded = useSelector((state) => (
    // @ts-ignore
    eventSelectors.isLoaded(state, eventId)
  ));
  const isOrganizer = useSelector((state) => (
    // @ts-ignore
    eventSelectors.isOrganizer(state, eventId)
  ));
  const name = useSelector((state) => (
    // @ts-ignore
    eventSelectors.selectName(state, eventId)
  ));
  const enrollments = useSelector((state) => (
    // @ts-ignore
    eventSelectors.hasEnrollmentsFeatureEnabled(state, eventId)
  ));

  const tabs = useMemo(() => {
    const result = [
      { key: 'assistance', label: t('Assistance'), icon: <Icon as={UserCheck} /> },
      { key: 'edit', label: t('Edit'), icon: <Icon as={Pencil} /> },
      { key: 'invites', label: t('Invites'), icon: <Icon as={Mail} /> },
      { key: 'bans', label: t('Bans'), icon: <Icon as={Ban} /> },
    ];

    if (enrollments) result.unshift({ key: 'enrollments', label: t('Enrollments'), icon: <Icon as={UserPlus} /> });

    return result;
  }, [t, enrollments]);

  const onTabChange = useCallback((index: number) => {
    history.replace(`/events/${eventId}/${tabs[index].key}`);
  }, [history, tabs, eventId]);

  if (!match || !isLoaded || !isOrganizer) return null;

  const tabIndex = tabs.findIndex(tab => tab.key === match.params.key);

  return (
    <Box>
      <PageHeader title={name} />

      {/* @ts-ignore */}
      <TabsWrapper sticky>
        <TabsList data={tabs} selected={tabIndex} onSelect={onTabChange} />
      </TabsWrapper>
    </Box>
  );
};

export default Header;
