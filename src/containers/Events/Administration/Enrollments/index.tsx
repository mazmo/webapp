/* eslint-disable @typescript-eslint/no-unused-vars */
import { useEffect, useState } from 'react';
import {
  Box, Table, Tbody, Td, Th, Thead, Tr,
} from '@chakra-ui/react';
import { useParams } from 'react-router-dom';

import api from 'state/api';
import { useTranslation } from 'hooks';

import Loading from 'components/Loading';
import UserDisplayName from 'components/UserDisplayName';

import UserLink from 'components/UserLink';
import useToast from 'hooks/useToast';
import locales from '../i18n';
import EnrollmentRow from './EnrollmentRow';

interface Props {
}

enum State {
  IDLE,
  LOADING,
  LOADED,
  ERROR,
}

const Enrollments: React.FC<Props> = () => {
  const { eventId } = useParams<{ eventId: string }>();
  const toast = useToast();
  const { t } = useTranslation(locales);

  const [fetchState, setFetchState] = useState(State.IDLE);
  const [enrollments, setEnrollments] = useState<any[]>([]);

  useEffect(() => {
    (async () => {
      if (fetchState === State.IDLE) {
        try {
          setFetchState(State.LOADING);
          const { data } = await api.req!.get(`/events/${eventId}/enrollments`);
          setEnrollments(data);
          setFetchState(State.LOADED);
        } catch (error) {
          setFetchState(State.ERROR);
          toast.error(error);
        }
      }
    })();
  }, [eventId, toast, fetchState]);

  const changeApproved = (userId: number, approved: boolean) => {
    setEnrollments((cv) => {
      const index = cv.findIndex((e) => e.userId === userId);
      if (index < 0) return cv;

      const enrollment = cv[index];
      const nv = [...cv];
      nv[index] = {
        ...cv[index],
        approved,
      };

      return nv;
    });
  };

  if (fetchState === State.LOADING) return <Loading />;

  return (
    <Box>
      <Table>
        <Thead>
          <Tr>
            <Th>{t('User')}</Th>
            <Th isNumeric>{t('Actions')}</Th>
          </Tr>
        </Thead>

        <Tbody>
          {enrollments.map((enrollment) => (
            <EnrollmentRow
              key={`enrollment-${enrollment.id}`}
              userId={enrollment.userId}
              approved={enrollment.approved}
              changeApproved={changeApproved}
            />
          ))}
        </Tbody>
      </Table>
    </Box>
  );
};

export default Enrollments;
