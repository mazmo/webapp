import { useState } from 'react';
import {
  Button, ButtonGroup, Td, Tr,
} from '@chakra-ui/react';
import { useParams } from 'react-router-dom';

import { useTranslation } from 'hooks';
import api from 'state/api';

import UserDisplayName from 'components/UserDisplayName';
import UserLink from 'components/UserLink';
import useToast from 'hooks/useToast';

import locales from '../i18n';

interface Props {
  userId: number;
  approved?: boolean;
  changeApproved: (uId: number, a: boolean) => void;
}

const EnrollmentRow: React.FC<Props> = ({ userId, approved, changeApproved }) => {
  const { t } = useTranslation(locales);
  const { eventId } = useParams<{ eventId: string }>();
  const toast = useToast();

  const [isApproving, setIsApproving] = useState(false);
  const [isRejecting, setIsRejecting] = useState(false);

  let color;
  if (approved === true) color = 'green.200';
  if (approved === false) color = 'red.200';

  const approve = async () => {
    try {
      setIsApproving(true);
      await api.req!.put(`/events/${eventId}/enrollments/${userId}/approve`);
      changeApproved(userId, true);
    } catch (error) {
      toast.error(error);
    } finally {
      setIsApproving(false);
    }
  };

  const reject = async () => {
    try {
      setIsRejecting(true);
      await api.req!.put(`/events/${eventId}/enrollments/${userId}/reject`);
      changeApproved(userId, false);
    } catch (error) {
      toast.error(error);
    } finally {
      setIsRejecting(false);
    }
  };

  return (
    <Tr bgColor={color}>
      <Td>
        <UserLink userId={userId}>
          <UserDisplayName userId={userId} />
        </UserLink>
      </Td>
      <Td isNumeric>
        <ButtonGroup size="sm">
          <Button colorScheme="green" isLoading={isApproving} isDisabled={isRejecting} onClick={approve}>{t('Approve')}</Button>
          <Button colorScheme="red" isLoading={isRejecting} isDisabled={isApproving} onClick={reject}>{t('Reject')}</Button>
        </ButtonGroup>
      </Td>
    </Tr>
  );
};

export default EnrollmentRow;
