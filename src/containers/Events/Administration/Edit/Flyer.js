import { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import loadImage from 'blueimp-load-image';
import { useSelector } from 'react-redux';
import { Box, Image, Button } from '@chakra-ui/react';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';

import FileSelector from 'components/FileSelector';

import FlyerModal from './FlyerModal';
import locales from '../i18n';

const Flyer = ({ eventId }) => {
  const { t } = useTranslation(locales);

  const flyer = useSelector(state => eventSelectors.selectFlyer(state, eventId));
  const name = useSelector(state => eventSelectors.selectName(state, eventId));

  const [newFlyer, setFlyer] = useState(null);
  const closeFlyerModal = useCallback(() => {
    setFlyer(null);
  }, []);

  const onFlyerChange = useCallback((e) => {
    loadImage(
      e.target.files[0],
      (img) => {
        const base64data = img.toDataURL('image/jpeg');
        setFlyer({
          src: base64data,
          img,
        });
      },
      {
        maxWidth: 600, maxHeight: 700, orientation: true, canvas: true,
      },
    );
  }, []);

  return (
    <>
      <Box textAlign="center" mb={8}>
        <Image src={flyer} alt={name} mx="auto" mb={2} maxWidth="100%" />

        <div className="edit-button">
          <FileSelector onChange={onFlyerChange}>
            <Button>{t('global:Change')}</Button>
          </FileSelector>
        </div>
      </Box>

      {/* Modals */}
      {newFlyer && <FlyerModal eventId={eventId} close={closeFlyerModal} flyer={newFlyer} />}
    </>
  );
};

Flyer.propTypes = {
  eventId: PropTypes.string.isRequired,
};

Flyer.defaultProps = {
};

export default Flyer;
