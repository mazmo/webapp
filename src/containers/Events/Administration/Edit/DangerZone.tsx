import React, { useState, useRef } from 'react';
import {
  Box,
  Button,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  Input,
  Text,
  useDisclosure,
} from '@chakra-ui/react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import * as eventSelectors from 'state/events/selectors';
import * as eventActions from 'state/events/actions';
import useToast from 'hooks/useToast';
import { useTranslation } from 'hooks';

import locales from '../i18n';

interface DangerZoneProps {
  eventId: string;
}

const DangerZone: React.FC<DangerZoneProps> = ({ eventId }) => {
  const history = useHistory();
  const toast = useToast();
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const { isOpen, onOpen, onClose } = useDisclosure();
  const [confirmText, setConfirmText] = useState('');
  const [isDeleting, setIsDeleting] = useState(false);
  const cancelRef = useRef<HTMLButtonElement>(null);

  const eventName = useSelector(
    // @ts-ignore
    state => eventSelectors.selectName(state, eventId),
  );

  const handleDelete = async () => {
    if (confirmText.toLowerCase() === 'delete') {
      try {
        setIsDeleting(true);
        await dispatch(eventActions.remove(eventId));
        toast.success(t('Event deleted'));
        setIsDeleting(false);
        history.push('/events');
      } catch (error) {
        setIsDeleting(false);
        toast.error(error);
      }
    }
  };

  return (
    <Box borderWidth={1} borderColor="red.500" p={4} borderRadius="md">
      <Text fontSize="xl" fontWeight="bold" mb={2}>
        {t('Danger Zone')}
      </Text>
      <Button colorScheme="red" onClick={onOpen}>
        {t('Delete Event')}
      </Button>

      <AlertDialog
        isOpen={isOpen}
        leastDestructiveRef={cancelRef}
        onClose={onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              {t('Delete Event')}
            </AlertDialogHeader>

            <AlertDialogBody>
              <Text mb={4}>
                {t('Are you sure you want to delete the event "{{eventName}}"? This action cannot be undone.', { eventName })}
              </Text>
              <Text mb={2}>{t('To confirm, type "delete" below')}</Text>
              <Input
                value={confirmText}
                onChange={(e) => setConfirmText(e.target.value)}
                placeholder={t('Type "delete" to confirm')}
              />
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={onClose} isDisabled={isDeleting}>
                {t('global:Cancel')}
              </Button>
              <Button
                colorScheme="red"
                onClick={handleDelete}
                ml={3}
                isDisabled={confirmText.toLowerCase() !== 'delete'}
                isLoading={isDeleting}
              >
                {t('Delete Event')}
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </Box>
  );
};

export default DangerZone;
