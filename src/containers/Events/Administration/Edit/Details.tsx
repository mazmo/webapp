import { useDispatch, useSelector } from 'react-redux';
import {
  Button, Stack,
  Input,
  Switch, NumberInput, NumberInputField, NumberInputStepper, NumberIncrementStepper, NumberDecrementStepper,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Select,
  Textarea,
  SimpleGrid,
  HStack,
  Icon,
  Divider,
} from '@chakra-ui/react';
import { Controller, useForm } from 'react-hook-form';
import { z } from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';
import { toZonedTime } from 'date-fns-tz';
import { format } from 'date-fns';
import { ChevronDown, ChevronUp } from 'lucide-react';

import { useTranslation } from 'hooks';
import useToast from 'hooks/useToast';
import * as eventSelectors from 'state/events/selectors';
import * as eventActions from 'state/events/actions';

import ToggleButtonGroup from 'components/ToggleButtonGroup';
import GoogleMap, { Location } from 'components/GoogleMap';
import Composer from 'components/Composer';

import locales from '../i18n';

type Props = {
  eventId: string;
};

const editEventSchema = z.object({
  name: z.string().min(1),
  rsvpLimit: z.string().optional(),
  address: z.object({
    streetNumber: z.string().optional(),
    route: z.string().optional(),
    intersection: z.string().optional(),
    adminArea: z.string().optional(),
    subdivision: z.string().optional(),
    country: z.string().optional(),
    lat: z.number().optional(),
    lng: z.number().optional(),
  }),
  location: z.object({
    type: z.enum(['Point', 'Virtual']),
    coordinates: z.array(z.number()).optional(),
    url: z.string().optional(),
  }),
  date: z.date(),
  description: z.string(),
  instructions: z.string().optional(),
  price: z.number(),
  currency: z.enum(['USD', 'EUR', 'ARS']),
  isPublic: z.boolean(),
  showLocation: z.boolean(),
});

type EditEventForm = z.infer<typeof editEventSchema>;

const Details: React.FC<Props> = ({ eventId }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const toast = useToast();

  const event = useSelector(
    // @ts-ignore
    state => eventSelectors.selectById(state, eventId),
  );

  const {
    register, handleSubmit, formState: { errors, isSubmitting }, watch, setValue, control,
  } = useForm<EditEventForm>({
    resolver: zodResolver(editEventSchema),
    defaultValues: {
      name: event.name,
      rsvpLimit: event.rsvpLimit?.toString() || undefined,
      address: {
        streetNumber: event.address?.streetNumber,
        route: event.address?.route,
        intersection: event.address?.intersection,
        adminArea: event.address?.adminArea,
        subdivision: event.address?.subdivision || undefined,
        country: event.address?.country,
        lat: event.location?.coordinates?.[0],
        lng: event.location?.coordinates?.[1],
      },
      location: {
        type: event.location?.type || 'Point',
        coordinates: event.location?.coordinates,
        url: event.location?.url,
      },
      date: toZonedTime(event.date, Intl.DateTimeFormat().resolvedOptions().timeZone),
      description: event.description,
      instructions: event.instructions,
      price: event.price,
      currency: event.currency,
      isPublic: event.isPublic,
      showLocation: event.showLocation,
    },
  });

  const currentRsvpLimit = watch('rsvpLimit');
  const currentLocationType = watch('location.type') as string;
  const currentAddress = watch('address');
  const currentCoordinates = watch('location.coordinates');
  const isLimitEnabled = typeof currentRsvpLimit !== 'undefined';
  const composerId = `eventedit-${eventId}`;

  const handleSwitchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!e.target.checked) {
      setValue('rsvpLimit', undefined);
    } else if (typeof currentRsvpLimit === 'undefined') {
      setValue('rsvpLimit', '100');
    }
  };

  const save = async (data: EditEventForm) => {
    try {
      const params = {
        ...data,
        rsvpLimit: data.rsvpLimit ? Number(data.rsvpLimit) : undefined,
        date: data.date.toISOString(),
        location: {
          ...data.location,
          coordinates: data.location.type === 'Point' ? [data.address.lat, data.address.lng] : undefined,
        },
      };
      await dispatch(eventActions.update(eventId, params));
      toast.success(t('Event updated'));
    } catch (error) {
      toast.error(error);
    }
  };

  return (
    <form onSubmit={handleSubmit(save)}>
      <Stack spacing={6}>
        <FormControl>
          <FormLabel htmlFor="name">{t('Name')}</FormLabel>
          <Input
            id="name"
            {...register('name', { required: true })}
          />
          {errors.name && (
            <FormErrorMessage>{errors.name.message}</FormErrorMessage>
          )}
        </FormControl>

        <Stack spacing={{ base: 6, md: 10 }} direction={{ base: 'column', md: 'row' }}>
          <FormControl flex={1}>
            <FormLabel htmlFor="date">{t('Date')}</FormLabel>
            <Controller
              name="date"
              control={control}
              render={({ field }) => (
                <Input
                  type="datetime-local"
                  {...field}
                  value={format(field.value, "yyyy-MM-dd'T'HH:mm")}
                  onChange={(e) => field.onChange(new Date(e.target.value))}
                />
              )}
            />
            <FormErrorMessage>{errors.date?.message}</FormErrorMessage>
          </FormControl>

          <HStack flex={1}>
            <FormControl flex={5}>
              <FormLabel htmlFor="price">{t('Price')}</FormLabel>
              <NumberInput>
                <NumberInputField id="price" {...register('price', { valueAsNumber: true })} />
              </NumberInput>
            </FormControl>

            <FormControl flex={2}>
              <FormLabel htmlFor="currency">{t('Currency')}</FormLabel>
              <Controller
                name="currency"
                control={control}
                render={({ field }) => (
                  <Select {...field}>
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                    <option value="ARS">ARS</option>
                  </Select>
                )}
              />
            </FormControl>
          </HStack>
        </Stack>

        <FormControl>
          <FormLabel htmlFor="description">{t('Description')}</FormLabel>
          <Composer
            id={composerId}
            height="250px"
            initialValue={event.description}
            onChange={(value) => setValue('description', value.getValue())}
          />
        </FormControl>

        <Divider />

        <SimpleGrid columns={{ base: 1, md: 3 }} spacing={4}>
          <FormControl>
            <FormLabel htmlFor="rsvpLimitSwitch">{t('RSVPs max')}</FormLabel>
            <HStack spacing={4}>
              <Switch
                isChecked={isLimitEnabled}
                onChange={handleSwitchChange}
                id="rsvpLimitSwitch"
              />
              <Controller
                name="rsvpLimit"
                control={control}
                render={({ field }) => (
                  <NumberInput
                    height="1.3rem"
                    min={1}
                    max={50000}
                    {...field}
                    size="xs"
                    w="100px"
                    isDisabled={!isLimitEnabled}
                    display={isLimitEnabled ? 'flex' : 'none'}
                  >
                    <NumberInputField />
                    <NumberInputStepper>
                      <NumberIncrementStepper>
                        <Icon as={ChevronUp} />
                      </NumberIncrementStepper>
                      <NumberDecrementStepper>
                        <Icon as={ChevronDown} />
                      </NumberDecrementStepper>
                    </NumberInputStepper>
                  </NumberInput>
                )}
              />
            </HStack>
          </FormControl>

          <FormControl>
            <FormLabel htmlFor="isPublic">{t('Public Event')}</FormLabel>
            <Switch id="isPublic" {...register('isPublic')} />
          </FormControl>

          <FormControl>
            <FormLabel htmlFor="showLocation">{t('Show Location')}</FormLabel>
            <Switch id="showLocation" {...register('showLocation')} />
          </FormControl>
        </SimpleGrid>

        <Divider />

        <ToggleButtonGroup
          buttons={[
            { label: t('Point'), key: 'Point' },
            { label: t('Virtual'), key: 'Virtual' },
          ]}
          onChange={(value: string) => setValue('location.type', value as 'Point' | 'Virtual')}
          selected={currentLocationType}
          mt={4}
        />

        <GoogleMap
          location={(
            Object.keys(currentAddress).length > 0
              ? { lat: currentCoordinates?.[0], lng: currentCoordinates?.[1], ...currentAddress } as Location
              : null
          )}
          setLocation={(address: Location | null) => setValue('address', address || {})}
          display={currentLocationType === 'Virtual' ? 'none' : 'unset'}
        />

        <FormControl display={currentLocationType === 'Virtual' ? 'unset' : 'none'}>
          <FormLabel htmlFor="location.url">{t('URL')}</FormLabel>
          <Input id="location.url" {...register('location.url')} />
        </FormControl>

        <FormControl>
          <FormLabel htmlFor="instructions">{t('Instructions')}</FormLabel>
          <Textarea id="instructions" {...register('instructions')} />
        </FormControl>

        <Button isLoading={isSubmitting} w="full" variant="primary" type="submit" mt={2}>{t('global:Save')}</Button>
      </Stack>
    </form>
  );
};

export default Details;
