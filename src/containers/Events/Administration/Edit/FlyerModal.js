import { useCallback, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { useDispatch } from 'react-redux';

import { useTranslation } from 'hooks';
import getCroppedImg from 'utils/getCroppedImg';
import * as eventActions from 'state/events/actions';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Button from 'components/Button';

import locales from '../i18n';

const FlyerModal = ({ eventId, close, flyer }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const imageDOM = useRef(null);
  const [loading, setLoading] = useState(false);
  const [crop, setCrop] = useState({
    x: 0,
    y: 0,
    width: 187,
    height: 250,
    aspect: 0.75,
  });

  const save = useCallback(async () => {
    try {
      setLoading(true);
      const croppedImg = await getCroppedImg(imageDOM.current, crop, 'temp.jpg');
      const uploadedFlyer = await dispatch(eventActions.uploadFlyer(croppedImg));
      await dispatch(eventActions.updateFlyer(eventId, uploadedFlyer.filename));
      close();
    } catch (error) {
      setLoading(false);
      dispatch(appActions.addError(error));
    }
  }, [dispatch, crop, eventId, close]);

  const imageLoaded = useCallback((img) => {
    imageDOM.current = img;
  }, [imageDOM]);

  const onCropChange = useCallback((data) => {
    setCrop(data);
  }, [setCrop]);

  return (
    <Modal
      title={t('Flyer')}
      onCancel={close}
      actions={[
        <Button key="event-edit-flyer" onClick={save} loading={loading}>{t('global:Confirm')}</Button>,
      ]}
    >
      <ReactCrop
        src={flyer.src}
        onChange={onCropChange}
        onImageLoaded={imageLoaded}
        crop={crop}
        minWidth={100}
        minHeight={100}
        keepSelection
      />
    </Modal>
  );
};

FlyerModal.propTypes = {
  eventId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  flyer: PropTypes.shape({
    src: PropTypes.string,
  }).isRequired,
};

FlyerModal.defaultProps = {
};

export default FlyerModal;
