import { useParams } from 'react-router-dom';
import { Divider, Stack } from '@chakra-ui/react';

import Flyer from './Flyer';
import Details from './Details';
import DangerZone from './DangerZone';

const Edit = () => {
  const { eventId } = useParams<{ eventId: string }>();

  return (
    <Stack spacing={8}>
      <Flyer eventId={eventId} />

      <Divider />

      <Details eventId={eventId} />

      <Divider />

      <DangerZone eventId={eventId} />
    </Stack>
  );
};

export default Edit;
