import { useEffect, useState } from 'react';
import {
  Redirect, Route, Switch, useParams,
} from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';
import * as eventActions from 'state/events/actions';

import NotFound from 'containers/NotFound';
import Loading from 'components/Loading';

import Error from './Error';
import Enrollments from './Enrollments';
import Assistance from './Assistance';
import Edit from './Edit';
import Bans from './Bans';
import Invites from './Invites';
import locales from './i18n';

const Content: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);
  const { eventId } = useParams<{ eventId: string }>();

  const isLoaded = useSelector((state) => (
    // @ts-ignore
    eventSelectors.isLoaded(state, eventId)
  ));
  const isOrganizer = useSelector((state) => (
    // @ts-ignore
    eventSelectors.isOrganizer(state, eventId)
  ));

  const [error, setError] = useState(null);

  useEffect(() => {
    const fetch = async () => {
      try {
        dispatch(eventActions.fetch(eventId));
      } catch (err: any) {
        setError(err);
      }
    };

    fetch();
  }, [dispatch, eventId]);

  if (!isLoaded) return <Loading />;
  if (error) {
    //
  }
  if (!isOrganizer) return <Error message={t('You are not an organizer of this Event')} />;

  return (
    <Switch>
      <Route path="/events/:eventId/enrollments" component={Enrollments} />
      <Route path="/events/:eventId/assistance" component={Assistance} />
      <Route path="/events/:eventId/edit" component={Edit} />
      <Route path="/events/:eventId/invites" component={Invites} />
      <Route path="/events/:eventId/bans" component={Bans} />
      <Route path="/events/:eventId" component={() => <Redirect to={`/events/${eventId}/assistance`} />} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default Content;
