import moment from 'moment';

import SimpleEvent from './SimpleEvent';
import FeaturedEvent from './FeaturedEvent';
import HighlightedEvent from './HighlightedEvent';
import { SITE_URL } from '../../../constants';

interface Props {
  data: {
    cover: string;
    name: string;
    description: string;
    links: string[];
    date: string;
    rsvpCount: number;
    price: number;
    currency: string;
    address: {
      subdivision: string;
      country: string;
    }
    features: {
      highlight: boolean;
      assistance: boolean;
    }
    location: {
      type?: 'Point' | 'Virtual';
    }
  };
}

const Event: React.FC<Props> = ({ data }) => {
  const city = (data?.location?.type === 'Virtual' || !data.address) ? 'Virtual' : `${data.address.subdivision}, ${data.address.country}`;
  const dateFormatted = moment(data.date).format('dddd D \\d\\e MMMM');
  const link = (data.links[0] || '').replace(SITE_URL, '');

  if (!data.features.highlight) {
    return <SimpleEvent name={data.name} link={link} date={dateFormatted} city={city} />;
  }

  if (!data.features.assistance) {
    return <HighlightedEvent name={data.name} link={link} date={dateFormatted} city={city} coverSrc={data.cover} />;
  }

  return (
    <FeaturedEvent
      name={data.name}
      description={data.description}
      coverSrc={data.cover}
      link={link}
      date={dateFormatted}
      city={city}
      price={data.price}
      currency={data.currency}
      rsvps={data.rsvpCount}
    />
  );
};

export default Event;
