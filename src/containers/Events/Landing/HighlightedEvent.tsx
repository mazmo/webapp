import {
  Box, Link as ChakraLink, HStack, Image, Text,
} from '@chakra-ui/react';
import { Link } from 'react-router-dom';

import { SITE_URL } from '../../../constants';

interface Props {
  name: string;
  link: string;
  date: string;
  city: string;
  coverSrc: string;
}

const HighlightedEvent: React.FC<Props> = ({
  name, link, date, city, coverSrc,
}) => {
  return (
    <ChakraLink as={Link} to={link.replace(SITE_URL, '')}>
      <HStack>
        <Image
          src={coverSrc}
          h={{ base: '50px' }}
          rounded="lg"
        />
        <Box>
          <Text noOfLines={1}>{name}</Text>
          <Text noOfLines={1} color="gray.500" fontSize="sm">{date}</Text>
          <Text noOfLines={1} color="gray.400" fontSize="xs">{city}</Text>
        </Box>
      </HStack>
    </ChakraLink>
  );
};

export default HighlightedEvent;
