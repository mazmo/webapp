import { useEffect, useMemo, useState } from 'react';
import {
  Box, HStack, Icon, IconButton, Menu, MenuButton, MenuItemOption, MenuList, MenuOptionGroup, Stack,
  Text,
  useTheme,
} from '@chakra-ui/react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { Filter, Plus } from 'lucide-react';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as eventSelectors from 'state/events/selectors';
import * as eventActions from 'state/events/actions';
import * as appSelectors from 'state/app/selectors';
import * as appActions from 'state/app/actions';
// import mapsLoader from 'utils/maps-loader';

import ErrorState from 'components/ErrorState';
import PageHeader from 'components/PageHeader';
import FloatingButton from 'components/Button/Floating';

import locales from './i18n';
import LoadingEvents from './Loading';
import Event from './Event';
import EventsEmptyState from './EmptyState';

const AUTOSELECT_KMS = 600;

const degreesToRadians = (degrees: number) => {
  return degrees * (Math.PI / 180);
};

const citiesInRange = (eventCoords: number[], userCoords: number[]): boolean => {
  const [lat1, lon1] = userCoords;
  const [lat2, lon2] = eventCoords;

  const R = 6371; // Radius of the Earth in kilometers

  const dLat = degreesToRadians(lat2 - lat1);
  const dLon = degreesToRadians(lon2 - lon1);

  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
    + Math.cos(degreesToRadians(lat1)) * Math.cos(degreesToRadians(lat2))
    * Math.sin(dLon / 2) * Math.sin(dLon / 2);

  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const distance = R * c;

  return distance <= AUTOSELECT_KMS;
};

const EventsContent: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);
  const theme = useTheme();

  const userCoordinates = useSelector(authSelectors.selectCoordinates);
  const events = useSelector(eventSelectors.selectUpcomingIds, shallowEqual);
  const uiLeftColumn = useSelector(appSelectors.selectIsUiLeftColumnActive);

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<unknown>(null);
  const [citiesFilter, setCitiesFilter] = useState<string[]>([]);

  useEffect(() => {
    const load = async () => {
      try {
        setLoading(true);
        await dispatch(eventActions.fetchAll());
      } catch (err) {
        setError(err);
      }

      setLoading(false);
    };

    load();
  }, [dispatch]);

  const cities = useMemo(() => {
    const values: Record<string, number> = {};
    (events || []).forEach((event) => {
      if (event.address) {
        const city = `${event.address.subdivision}, ${event.address.country}`;
        if (!values[city]) values[city] = 0;
        values[city] += 1;
      } else if (event?.location?.type === 'Virtual') {
        if (!values.Virtual) values.Virtual = 0;
        values.Virtual += 1;
      }
    });
    return values;
  }, [events]);

  useEffect(() => {
    const selectInitialCities = async () => {
      if (events) {
        setCitiesFilter(events.filter((event) => {
          if (event?.location?.type === 'Virtual' || !event.address?.latLng || !userCoordinates) return true;
          return citiesInRange(event.address.latLng, userCoordinates);
        }).map((event) => {
          if (event?.location?.type === 'Virtual' || !event.address?.latLng) return 'Virtual';
          return `${event.address.subdivision}, ${event.address.country}`;
        }));
      }
    };

    selectInitialCities();
  }, [events, cities, userCoordinates]);

  useEffect(() => {
    dispatch(appActions.uiLeftColumn(true));
  }, [uiLeftColumn, dispatch]);


  if (loading) return <LoadingEvents />;
  if (error) return <ErrorState />;

  const onCitiesFilterChange = (rawValues: string | string[]) => {
    const values = !Array.isArray(rawValues) ? [rawValues] : rawValues;
    setCitiesFilter(values);
  };

  const sortedAndFilteredEvents = events
    .sort((a, b) => {
      if (a.features.assistance && !b.features.assistance) return -1;
      if (b.features.assistance && !a.features.assistance) return 1;
      if (a.features.highlight && !b.features.highlight) return -1;
      if (b.features.highlight && !a.features.highlight) return 1;
      return new Date(a.date).getTime() - new Date(b.date).getTime();
    })
    .filter((event) => {
      const city = (event?.location?.type === 'Virtual' || !event.address) ? 'Virtual' : `${event.address.subdivision}, ${event.address.country}`;
      return citiesFilter.includes(city);
    });

  return (
    <Box position="relative">
      <PageHeader title={t('Next Events')}>
        <Menu closeOnSelect={false}>
          <MenuButton as={IconButton} aria-label="Filter" icon={<Icon as={Filter} />} size="sm" />
          <MenuList>
            <MenuOptionGroup title={t('Cities')} type="checkbox" onChange={onCitiesFilterChange} value={citiesFilter}>
              {Object.entries(cities).map(([city]) => (
                <MenuItemOption key={`city-${city}`} value={city}>
                  <HStack justifyContent="space-between" spacing={4}>
                    <Text>{city}</Text>
                    <Text fontSize="xs" px={2} bgColor="gray.200" color="gray.500" rounded="8px">{cities[city]}</Text>
                  </HStack>
                </MenuItemOption>
              ))}
            </MenuOptionGroup>
          </MenuList>
        </Menu>
      </PageHeader>

      {!sortedAndFilteredEvents.length
        ? <EventsEmptyState />
        : (
          <Stack spacing={4}>
            {sortedAndFilteredEvents.map((event) => (
              <Event key={`event-${event.id}`} data={event} />
            ))}
          </Stack>
        )
      }

      <FloatingButton
        to="/events/create"
        color={theme.colors.brand['500']}
        // @ts-ignore
        distance="16px"
        negative
      >
        <Icon as={Plus} color="white" />
      </FloatingButton>

    </Box>
  );
};

export default EventsContent;
