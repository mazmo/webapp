import { Link as ChakraLink, Text } from '@chakra-ui/react';
import { Link } from 'react-router-dom';

import { SITE_URL } from '../../../constants';

interface Props {
  name: string;
  link: string;
  date: string;
  city: string;
}

const SimpleEvent: React.FC<Props> = ({
  name, link, date, city,
}) => {
  return (
    <ChakraLink as={Link} to={link.replace(SITE_URL, '')}>
      <Text noOfLines={1}>{name}</Text>
      <Text noOfLines={1} color="gray.500" fontSize="sm">{date}</Text>
      <Text noOfLines={1} color="gray.400" fontSize="xs">{city}</Text>
    </ChakraLink>
  );
};

export default SimpleEvent;
