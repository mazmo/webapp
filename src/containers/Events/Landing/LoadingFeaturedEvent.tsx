import {
  Card, CardBody, CardFooter, Divider, HStack, Skeleton, Stack,
} from '@chakra-ui/react';

const LoadingFeaturedEvent: React.FC = () => {
  return (
    <Card>
      <CardBody overflow="hidden">
        <Skeleton
          w={{ base: '225px', md: '278px' }}
          h={{ base: '300px', md: '370px' }}
          mx="auto"
        />

        <Stack spacing={3} mt={6}>
          <Stack spacing={0}>
            <Skeleton h={{ base: '19px', md: '24px' }} w="90%" />
            <Skeleton h="16px" w="45%" />
          </Stack>

          <Skeleton h="60px" />

          <HStack justifyContent="space-between" mt={2}>
            <Skeleton h="28px" w="130px" />
          </HStack>
        </Stack>
      </CardBody>
      <Divider />
      <CardFooter>
        <Skeleton w="110px" h="40px" />
      </CardFooter>
    </Card>
  );
};

export default LoadingFeaturedEvent;
