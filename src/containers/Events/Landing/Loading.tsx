import { Stack, Skeleton, Box } from '@chakra-ui/react';

import { useTranslation } from 'hooks';

import PageHeader from 'components/PageHeader';

import locales from './i18n';
import LoadingFeaturedEvent from './LoadingFeaturedEvent';

const LoadingEvents: React.FC<React.PropsWithChildren> = () => {
  const { t } = useTranslation(locales);

  return (
    <Box>
      <PageHeader title={t('Next Events')} />
      <Stack spacing={4}>
        <LoadingFeaturedEvent />
        <LoadingFeaturedEvent />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
        <Skeleton height="24px" />
      </Stack>
    </Box>
  );
};

export default LoadingEvents;
