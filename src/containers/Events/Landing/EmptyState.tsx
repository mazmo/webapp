import { Box, Text } from '@chakra-ui/react';

import { useTranslation } from 'hooks';

import locales from './i18n';

const EventsEmptyState: React.FC = () => {
  const { t } = useTranslation(locales);

  return (
    <Box bgColor="gray.50" mt={6} textAlign="center" py={6} px={2} rounded="sm">
      <Text fontSize="xl" lineHeight={1}>{t('There are no events in your area right now')}</Text>
      <Text mt={4} color="gray.500">{t('Try changing the filters above')}</Text>
    </Box>
  );
};

export default EventsEmptyState;
