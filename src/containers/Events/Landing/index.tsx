import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import Layout from 'components/Layout';
import Sidebar from 'containers/Sidebar';

import Content from './Content';

const Landing: React.FC = () => (
  <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
    {/* @ts-ignore */}
    <FlexWrapper canOverflow>
      {/* @ts-ignore */}
      <FlexInnerWrapper>
        {/* @ts-ignore */}
        <FlexContainer framed>
          <Content />
        </FlexContainer>
      </FlexInnerWrapper>
    </FlexWrapper>

    <Sidebar />
  </Layout>
);

export default Landing;
