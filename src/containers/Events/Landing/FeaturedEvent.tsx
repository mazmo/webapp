import {
  Button,
  ButtonGroup,
  Card, CardBody, CardFooter, Divider, Heading, HStack, Icon, Image, Stack, Text,
} from '@chakra-ui/react';

import { useTranslation } from 'hooks';
import { toCurrency } from 'utils/toCurrency';

import ParsedContent from 'components/ParsedContent';

import { CalendarArrowUp, CalendarDays } from 'lucide-react';
import { Link } from 'react-router-dom';
import locales from './i18n';

interface Props {
  name: string;
  description: string;
  coverSrc: string;
  link: string;
  date: string;
  city: string;
  price: number;
  currency: string;
  rsvps: number;
}

const FeaturedEvent: React.FC<Props> = ({
  name, description, coverSrc, price, currency, date, rsvps, link,
}) => {
  const { t } = useTranslation(locales);

  return (
    <Card>
      <CardBody overflow="hidden">
        <Link to={link}>
          <Image
            src={coverSrc}
            alt={name}
            borderRadius="lg"
            w={{ base: '225px', md: '278px' }}
            h={{ base: '300px', md: '370px' }}
            mx="auto"
          />
        </Link>

        <Stack spacing={3} mt={6}>
          <Stack spacing={0}>
            <Link to={link}>
              <Heading size={{ base: 'sm', md: 'md' }} noOfLines={1}>{name}</Heading>
              <HStack>
                <Icon as={CalendarDays} color="gray.500" />
                <Text color="gray.500" fontSize="sm">{date}</Text>
              </HStack>
            </Link>
          </Stack>

          <Text noOfLines={3}>
            <ParsedContent content={description} markdown={false} />
          </Text>

          <HStack justifyContent="space-between" mt={2}>
            <Text color="brand.600" fontSize="2xl">
              {price > 0 ? toCurrency(price, currency) : t('Free')}
            </Text>

            <HStack>
              <Icon as={CalendarArrowUp} />
              <Text>{rsvps}</Text>
            </HStack>
          </HStack>
        </Stack>
      </CardBody>
      <Divider />
      <CardFooter>
        <ButtonGroup spacing="2">
          <Button as={Link} to={link} variant="primary">
            {t('View Event')}
          </Button>
        </ButtonGroup>
      </CardFooter>
    </Card>
  );
};

export default FeaturedEvent;
