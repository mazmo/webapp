import {
  Checkbox,
  CheckboxGroup,
  FormControl, FormLabel, Radio, RadioGroup, Stack, Textarea,
} from '@chakra-ui/react';

export type Question = {
  _id: string;
  question: string;
  options?: string[];
  type: 'TEXT' | 'SINGLE_CHOICE' | 'MULTIPLE_CHOICE' | 'RANK';
  step: number;
};

interface Props {
  data: Question;
  onChange: (value: string) => void;
}

const QuestionControl: React.FC<Props> = ({ data, onChange }) => {
  return (
    <FormControl>
      <FormLabel>{data.question}</FormLabel>

      {data.type === 'TEXT' && (
        <Textarea placeholder="Escribe tu respuesta aquí..." onChange={(e) => onChange(e.target.value)} />
      )}
      {data.type === 'SINGLE_CHOICE' && (
        <RadioGroup onChange={onChange}>
          <Stack>
            {data.options?.map((option) => (
              <Radio key={option} value={option}>
                {option}
              </Radio>
            ))}
          </Stack>
        </RadioGroup>
      )}
      {data.type === 'MULTIPLE_CHOICE' && (
        <CheckboxGroup onChange={(v) => onChange(v.join('\n'))}>
          <Stack>
            {data.options?.map((option) => (
              <Checkbox key={option} value={option}>
                {option}
              </Checkbox>
            ))}
          </Stack>
        </CheckboxGroup>
      )}
      {data.type === 'RANK' && (
        <RadioGroup onChange={onChange}>
          <Stack>
            {Array.from({ length: 5 }).map((_, i) => (
              // eslint-disable-next-line react/no-array-index-key
              <Radio key={`${data._id}-${i}`} value={String(i + 1)}>
                {Array.from({ length: i + 1 }).map(() => '⭐️').join('')}
              </Radio>
            ))}
          </Stack>
        </RadioGroup>
      )}
    </FormControl>
  );
};

export default QuestionControl;
