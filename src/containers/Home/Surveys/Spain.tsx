import { useEffect, useState } from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  ButtonGroup,
  HStack,
  Text,
  VStack,
} from '@chakra-ui/react';
import { useSelector } from 'react-redux';
import Cookies from 'js-cookie';
import moment from 'moment';

import Api from 'state/api';
import * as authSelectors from 'state/auth/selectors';
import useToast from 'hooks/useToast';

import ParsedContent from 'components/ParsedContent';

import QuestionControl, { Question } from './Question';

interface Props {
}

interface Survey {
  id: string;
  name: string;
  intro: string;
  questions: Question[];
}

const cookieName = 'survey-spain-01';

const Spain: React.FC<Props> = () => {
  const toast = useToast();

  const [survey, setSurvey] = useState<Survey>();
  const [step, setStep] = useState(0);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [answers, setAnswers] = useState<{ [key: string]: string }>({});

  const { displayname, regdate, country } = useSelector(authSelectors.selectMe);

  useEffect(() => {
    const fetchSurvey = async () => {
      const { data } = await Api.req!.get('/surveys/664ca22ece107b51a8ea3505');

      setSurvey(data);
    };

    const alreadyShown = Cookies.get(cookieName);
    if (!alreadyShown || (country && country.name === 'España')) fetchSurvey();
  }, [country]);

  const onClose = () => {
    setSurvey(undefined);
  };

  const notInterested = () => {
    Cookies.set(cookieName, '1');
    onClose();
  };

  const addStep = (n: number) => () => {
    setStep((cv) => cv + n);
  };

  const onChange = (questionId: string) => (value: string) => {
    setAnswers((cv) => ({
      ...cv,
      [questionId]: value,
    }));
  };

  const onSubmit = async () => {
    try {
      if (!survey) return;

      setIsSubmitting(true);
      await Api.req!.post(`/surveys/${survey.id}/answers`, {
        answers: Object.entries(answers).map(([questionId, answer]) => ({ question: questionId, answer })),
      });

      toast.success('Tus respuestas han sido enviadas. ¡Gracias por tu tiempo!');
      setIsSubmitting(false);
      notInterested();
    } catch (error) {
      toast.error(error);
      setIsSubmitting(false);
    }
  };

  if (!survey) return null;
  if (moment().diff(regdate, 'days') < 7) return null;
  if (!country || country.name !== 'España') return null;

  const totalSteps = Math.max(...survey.questions.map(q => q.step));
  const questions = survey.questions.filter(q => q.step === step);
  const allQuestionsAnswered = questions.every(q => answers[q._id]);

  return (
    <Modal isOpen onClose={onClose} size={{ base: 'full', md: '2xl' }}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>{survey.name}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          {step === 0 && (
            <ParsedContent content={survey.intro.replace('{{displayname}}', displayname)} />
          )}
          {step > 0 && (
            <VStack spacing={8}>
              {questions.map((question) => (
                <QuestionControl data={question} key={question._id} onChange={onChange(question._id)} />
              ))}
            </VStack>
          )}
        </ModalBody>

        <ModalFooter>
          {step === 0 && (
            <ButtonGroup>
              <Button onClick={notInterested}>No me interesa</Button>
              <Button variant="primary" onClick={addStep(1)}>Comenzar cuestionario</Button>
            </ButtonGroup>
          )}
          {step > 0 && (
            <HStack justifyContent="space-between" w="full">
              <Text fontSize="sm" textColor="gray.400">{`Paso ${step} de ${totalSteps}`}</Text>
              {step === totalSteps && (
                <ButtonGroup>
                  <Button variant="primary" onClick={onSubmit} isLoading={isSubmitting} isDisabled={!allQuestionsAnswered}>Enviar respuestas</Button>
                </ButtonGroup>
              )}
              {step > 0 && step < totalSteps && (
                <ButtonGroup>
                  <Button onClick={addStep(+1)} isDisabled={!allQuestionsAnswered}>Siguiente</Button>
                </ButtonGroup>
              )}
            </HStack>
          )}
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default Spain;
