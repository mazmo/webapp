export default {
  variants: {
    outline: {
      border: '1px solid',
      borderColor: 'gray.200',
      _hover: {
        borderColor: 'black',
      },
      _focus: {
        borderColor: 'black',
        boxShadow: '0 0 0 1px #EEE',
      },
    },
  },
};
