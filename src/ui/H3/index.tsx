import { Heading, HeadingProps } from '@chakra-ui/react';
import React from 'react';

interface Props extends HeadingProps {
  children: React.ReactNode;
}

const H3: React.FC<Props> = ({ children, size = 'md', ...props }) => (
  <Heading as="h3" size={size} mb={4} {...props}>
    {children}
  </Heading>
);

export default H3;
