const getCroppedImg = (img, c, fileName) => {
  const canvas = document.createElement('canvas');
  const scaleX = img.naturalWidth / img.width;
  const scaleY = img.naturalHeight / img.height;
  canvas.width = c.width;
  canvas.height = c.height;
  const ctx = canvas.getContext('2d');

  ctx.drawImage(
    img,
    c.x * scaleX,
    c.y * scaleY,
    c.width * scaleX,
    c.height * scaleY,
    0,
    0,
    c.width,
    c.height,
  );

  return new Promise((resolve, reject) => {
    canvas.toBlob((blob) => {
      if (!blob) {
        reject(new Error('Something went wrong'));
      } else {
        // eslint-disable-next-line no-param-reassign
        blob.name = fileName;
        resolve(blob);
      }
    }, 'image/jpeg');
  });
};

export default getCroppedImg;
