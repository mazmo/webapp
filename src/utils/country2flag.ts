export default function country2flag(countryCode: string = ''): string {
  if (countryCode === '') return '';
  return countryCode
    .toUpperCase()
    .split('')
    .map(char => String.fromCodePoint(char.charCodeAt(0) + 0x1F1A5))
    .join('');
}
