import { Loader } from '@googlemaps/js-api-loader';

import { GMAPS_API_KEY } from '../constants';

const loaderInstance = new Loader({
  apiKey: GMAPS_API_KEY,
  version: 'weekly',
  libraries: ['places', 'geometry'], // Include all required libraries here
});

export default loaderInstance;
