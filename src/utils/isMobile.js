import ismobile from 'ismobilejs';

export default ismobile.phone || ismobile.tablet;
