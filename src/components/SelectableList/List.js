import styled from 'styled-components';

const SelectableList = styled.ul`
  overflow-y: auto;
  -webkit-overflow-scrolling: touch;
  height: 100%;
`;

export default SelectableList;
