import { forwardRef } from 'react';
import styled from 'styled-components';

const InputWrapper = styled.div`
  display: flex;
  position: relative;
`;

const Icon = styled.div`
  display: flex;
  width: 18px;
  height: 18px;
  position: absolute;
  top: calc(50% - 18px / 2);
  left: 16px;
  svg {
    width: 100%;
    height: 100%;
  }
`;

const InputComponent = styled.input.attrs({
  type: 'search',
})<React.HTMLProps<HTMLInputElement>>`
  font-size: 16px;
  font-weight: 500;
  line-height: 56px;
  width: 100%;
  margin: 0;
  padding: 0 24px 0 48px;
  border: 1px solid #F5F0F0;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.04), 0px 4px 8px rgba(0, 0, 0, 0.08);
  border-radius: 8px;
  transition: all 250ms ease-out;
  &:focus {
    outline: none;
  }
`;

interface Props extends React.HTMLProps<HTMLInputElement> {
  placeholder?: string;
}

const InputSearch = forwardRef(({
  onChange,
  placeholder,
  ...rest
}: Props, ref: React.Ref<HTMLInputElement>) => (
  <InputWrapper>
    <Icon>
      <svg viewBox="0 0 18 18">
        <path
          d="M12.8645 11.3208H12.0515L11.7633 11.0429C12.7719 9.86964 13.3791 8.34648 13.3791 6.68954C13.3791 2.99485 10.3842 0 6.68954 0C2.99485 0 0 2.99485 0 6.68954C0 10.3842 2.99485 13.3791 6.68954 13.3791C8.34648 13.3791 9.86964 12.7719 11.0429 11.7633L11.3208 12.0515V12.8645L16.4666 18L18 16.4666L12.8645 11.3208ZM6.68954 11.3208C4.12693 11.3208 2.05832 9.25214 2.05832 6.68954C2.05832 4.12693 4.12693 2.05832 6.68954 2.05832C9.25214 2.05832 11.3208 4.12693 11.3208 6.68954C11.3208 9.25214 9.25214 11.3208 6.68954 11.3208Z"
          fill="#A8A8A8"
        />
      </svg>
    </Icon>
    <InputComponent
      ref={ref}
      onChange={onChange}
      placeholder={placeholder}
      {...rest}
    />
  </InputWrapper>
));

InputSearch.displayName = 'InputSearch';

export default InputSearch;
