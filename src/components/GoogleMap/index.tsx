// eslint-disable-next-line spaced-comment

import {
  useRef, useEffect, useState, useCallback,
} from 'react';
import {
  Box, Button, HStack, Input, BoxProps,
} from '@chakra-ui/react';
import iso31662 from 'iso-3166-2';
import { MarkerWithLabel } from '@googlemaps/markerwithlabel';

import { useTranslation } from 'hooks';
import mapsLoader from 'utils/maps-loader';

import locales from './i18n';
import LocationName from './LocationName';


export type Location = {
  adminArea?: string;
  adminAreaLong?: string;
  subdivision?: string;
  country?: string;
  countryLong?: string;
  lat?: number;
  lng?: number;
  intersection?: string;
  route?: string;
  streetNumber?: string;
}

interface Props extends BoxProps {
  location: Location | null;
  setLocation: (location: Location | null) => void;
}

export const GoogleMap: React.FC<Props> = ({
  location, setLocation, ...boxProps
}) => {
  const { t } = useTranslation(locales);

  const mapElement = useRef(null);
  const autocompleteElement = useRef<HTMLInputElement | null>(null);
  // eslint-disable-next-line no-undef
  const autocomplete = useRef<google.maps.places.Autocomplete | null>(null);

  const [address, setAddress] = useState('');
  const [error, setError] = useState(false);

  const addressChanged = useCallback((e: React.ChangeEvent<HTMLInputElement>) => setAddress(e.target.value), []);
  const clearLocation = useCallback(() => {
    setLocation(null);
    setError(false);
    setAddress('');
    setTimeout(() => autocompleteElement.current?.focus(), 0);
  }, [setLocation]);

  useEffect(() => {
    const load = async () => {
      await mapsLoader.load();

      // Map
      if (mapElement.current && autocompleteElement.current) {
        const map = new global.google.maps.Map(mapElement.current, {
          center: { lat: location?.lat || 0, lng: location?.lng || 0 },
          zoom: location ? 16 : 20,
          disableDefaultUI: true,
        });

        // Marker
        const marker = new MarkerWithLabel({
          map,
          position: new global.google.maps.LatLng(location?.lat || 0, location?.lng || 0),
          labelContent: '',
        });

        // Autocomplete
        autocomplete.current = new global.google.maps.places.Autocomplete(
          autocompleteElement.current,
          { types: ['geocode'] },
        );
        autocomplete.current.setFields(['address_components', 'geometry', 'icon', 'name']);
        autocomplete.current.addListener('place_changed', () => {
          marker.setVisible(false);
          const place = autocomplete.current!.getPlace();

          if (!place.geometry?.location || !place.address_components) {
            setError(true);
            return;
          }

          map.setCenter(place.geometry.location);
          map.setZoom(17);
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          const streetNumber = place.address_components.find(c => c.types.includes('street_number'));
          const route = place.address_components.find(c => c.types.includes('route'));
          const intersection = place.address_components.find(c => c.types.includes('intersection'));
          const adminArea = place.address_components.find(c => c.types.includes('administrative_area_level_1')) || place.address_components.find(c => c.types.includes('administrative_area_level_2'));
          const country = place.address_components.find(c => c.types.includes('country'));

          if (!adminArea || !country) {
            setError(true);
            setLocation(null);
          } else {
            let adminAreaObj = iso31662.subdivision(country.short_name, adminArea.long_name);
            if (!adminAreaObj) {
              adminAreaObj = iso31662.subdivision(country.short_name, adminArea.short_name);
            }

            const newLocation: Location = {
              adminArea: adminAreaObj?.code,
              adminAreaLong: adminArea.long_name,
              country: iso31662.country(country.short_name)?.code,
              countryLong: country.long_name,
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng(),
              intersection: intersection ? intersection.short_name : undefined,
              route: route ? route.short_name : undefined,
              streetNumber: streetNumber ? streetNumber.short_name : undefined,
            };

            setLocation(newLocation);
          }
        });
      }
    };

    if (mapElement.current) {
      load();
    }

    if (autocompleteElement.current) {
      autocompleteElement.current.focus();
    }
  }, [setLocation, location]);

  return (
    <Box {...boxProps}>
      <Input
        ref={autocompleteElement}
        type="text"
        placeholder={t('Search for a location')}
        value={address}
        onChange={addressChanged}
        style={{ display: (location === null && !error) ? 'block' : 'none' }}
      />

      {location !== null && (
        <HStack justify="space-between" mb={2}>
          <LocationName
            streetNumber={location.streetNumber || ''}
            intersection={location.intersection || ''}
            adminAreaLong={location.adminAreaLong || location.subdivision || location.adminArea || ''}
            countryLong={location.countryLong || location.country || ''}
            route={location.route || ''}
          />
          <Button onClick={clearLocation} size="sm">{t('Change')}</Button>
        </HStack>
      )}

      <div ref={mapElement} style={{ width: '100%', height: location === null ? '0px' : '500px' }} />

      {error && (
        <div className="error">
          {t('We couldn\'t find a correct address for the location you entered. Are you sure you entered it correctly?')}
          <br />
          <br />
          <Button onClick={clearLocation}>{t('Change')}</Button>
        </div>
      )}
    </Box>
  );
};

export default GoogleMap;
