import { Text } from '@chakra-ui/react';

interface Props {
  streetNumber: string;
  intersection: string;
  adminAreaLong: string;
  countryLong: string;
  route: string;
}

const LocationName: React.FC<Props> = ({
  streetNumber, intersection, route, adminAreaLong, countryLong,
}) => {
  const locationSteps = [];

  if (streetNumber) {
    locationSteps.push(`${route} ${streetNumber}`);
  }

  if (intersection) {
    locationSteps.push(intersection);
  }

  locationSteps.push(`${adminAreaLong}, ${countryLong}`);

  return (
    <Text>
      {locationSteps.join(', ')}
    </Text>
  );
};

export default LocationName;
