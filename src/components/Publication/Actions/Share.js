import { useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import ReactTooltip from 'react-tooltip';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';

import ShareVariant from 'components/Icons/ShareVariant';
import ActionIcon from 'components/ActionIcon';
import Hoverable from 'components/Hoverable';
import ShareIcons from 'components/ShareIcons';

import { ActionWrapper } from '../UI';
import locales from '../i18n';
import { PUBLICATION_PRIVACIES, SITE_URL } from '../../../constants';

const Share = ({ publicationId }) => {
  const { t } = useTranslation(locales);
  const theme = useContext(ThemeContext);

  const privacy = useSelector(
    state => feedSelectors.publications.selectPrivacy(state, publicationId),
  );

  if (privacy !== PUBLICATION_PRIVACIES.PUBLIC) return null;

  const tooltipId = `publication-tooltip-${publicationId}`;
  const canonical = `${SITE_URL}/publications/${publicationId}`;

  return (
    <ActionWrapper>
      <ReactTooltip effect="solid" id={tooltipId} clickable event="click">
        <ShareIcons url={canonical} />
      </ReactTooltip>

      <ActionIcon data-tip data-for={tooltipId}>
        <Hoverable
          normal={<ShareVariant color={theme.colors.main} publicationId={publicationId} outline />}
          hover={<ShareVariant color={theme.colors.main} publicationId={publicationId} />}
          tooltip={t('Share')}
        />
      </ActionIcon>
    </ActionWrapper>
  );
};

Share.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Share.defaultProps = {
};

export default Share;
